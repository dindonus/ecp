<?php

/** EcranPlat - Light & fast backend framework **/

mb_internal_encoding('UTF-8');

define('PATH', mb_substr(__DIR__, 0, -3));
define('MICRO_TIME', microtime(true));
define('CLI', php_sapi_name() === 'cli');
define('XHR', getenv('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest');

ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('log_errors', true);
ini_set('error_log', PATH.'logs/php'.(CLI ? '-cli' : '').'.log');

set_error_handler('ECP::handleError');
set_exception_handler('ECP::handleException');

require(PATH.'ecp/core/functions.php');

try {
	$config = ECP::loadConfig();

	define('PROJECT', $config['project']['name']);
	define('DEBUG', $config['project']['debug']);
	define('URL', $config['project']['url']);
	define('DOMAIN', getDomaine(URL));
	define('HOST', getHost(URL));
	define('SESSION_NAME', $config['session'] ? $config['session']['name'] : null);
	define('SOCKET_URL', $config['socket']['url']);
	define('TOKEN', getToken(8));

	ini_set('display_errors', DEBUG or CLI);
	ini_set('date.timezone', $config['project']['timezone']);

	ECP::handleCLI();
	ECP::loadPage();

	$page = new Page();
	$page->run();
}
catch (Throwable $exception) {
	ECP::handleException($exception);
}

/**
 * ECPCore
 *
 * Objet commun aux classes EcranPlat, Package et Plugin.
 * Fournit :
 * - accès tables des bases de données
 * - accès aux packages
 * - accès aux classes d'informations
 * - accès aux plugins
 */
abstract class ECPCore {

	protected function table($table) {
		return ECP::getBase()->table($table);
	}

	protected function package($package) {
		return ECP::factory('package', $package);
	}

	protected function info($info) {
		return ECP::getInfo($info);
	}

	protected function plugin($plugin) {
		return ECP::factory('plugin', $plugin);
	}

}

/**
 * EcranPlat
 *
 * Parent de la class Projet.
 * Fournit l'ensemble des méthodes pour gérer le projet et les pages.
 */
abstract class EcranPlat extends ECPCore {

	protected $bus = [];
	protected $json = [];
	protected $monitoring = true;

	protected $meta = [];
	protected $css = [];
	protected $js = [];
	protected $ecpLoadAssets = true;
	protected $titre = null;
	protected $favicon = null;
	protected $touchIcon = false;
	protected $touchStartup = false;
	protected $antiCache = 1;

	public function run() {

		$this->sessionStart();

		define('MONITORING', ECP::hasMonitoring($this->monitoring));
		define('MONITORING_SQL', MONITORING and ECP::$config['sql']['monitoring']);

		if (MONITORING === true) {
			$this->plugin('monitoring')->handleStart();
		}

		if (XHR === true) {
			$param = file_get_contents('php://input');
			$param = json_decode($param, true);

			if ($param !== null) {
				foreach ($param as $key => $value) {
					$_POST[ $key ] = $value;
				}
			}
		}

		Config::prepare();
		$action = $this->preparePrincipal();

		if (MONITORING === true) {
			$this->plugin('monitoring')->handlePrepare();
		}

		if (ECP::hasBase() === true and MONITORING === false) {
			ECP::getBase()->stop();
		}

		if ($action instanceof Page404) {
			header("HTTP/1.1 404 Not Found", false, 404);
			$action = null;
		}

		if (XHR === true) {
			header("Cache-Control: no-cache");
		}

		if ($action === null) {

			$this->printHeader();
			$this->affichePrincipal();

			if (DEBUG === true) {
				$this->profileBar();
			}

			$this->piedHtml();

		}
		else {

			if ($action instanceof Redirection) {

				if ($action->permanent === true and XHR === false) {
					header("HTTP/1.1 301 Moved Permanently");
				}

				$location = $action->content;

				if (mb_substr($location, 0, 7) !== 'http://' and mb_substr($location, 0, 8) !== 'https://') {
					$location = URL.$location;
				}

				if (XHR === true) {
					$this->json(['redirection' => $location]);
				}
				else {
					header("Location: $location");
				}

			}
			else if ($action instanceof Json) {

				$this->json($action->content);

			}
			else if ($action instanceof Png) {

				header("Content-Type: image/png");
				echo $action->content;

			}
			else if ($action instanceof Jpeg) {

				header("Content-Type: image/jpeg");
				echo $action->content;

			}
			else if ($action instanceof Gif) {

				header("Content-Type: image/gif");
				echo $action->content;

			}
			else if ($action instanceof Webm) {

				header("Content-Type: video/webm");
				echo $action->content;

			}
			else if ($action instanceof Stop) {
			}
			else if ($action instanceof Alert) {

				$this->json(['alert' => $action->content]);

			}
			else if ($action instanceof Failure) {

				$this->json(['error' => $action->content]);

			}
			else if ($action instanceof Success) {

				$this->json(['success' => $action->content]);

			}
			else {
				throw new Exception("Votre méthode 'preparePrincipal' doit renvoyer une action valide ou null.");
			}

		}

		if (MONITORING === true) {
			$this->plugin('monitoring')->handleEnd();
		}

	}

	protected function json($content) {
		header('Content-type: application/json; charset=UTF-8');
		echo json_encode($content, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
	}

	protected function preparePrincipal() {
		throw new Exception("Veuillez redéfinire la méthode 'preparePrincipal' dans votre projet.");
	}

	protected function affichePrincipal() {
		throw new Exception("Veuillez redéfinire la méthode 'affichePrincipal' dans votre projet.");
	}

	/* ------------------------ Méthodes privées ---------------------------- */

	private function sessionStart() {

		$params = ECP::$config['session'];

		if (CLI === true or !$params) {
			return ;
		}

		if ($params['memcached'] === true) {

			$_SESSION = null;

			if (autoCookie(SESSION_NAME) !== null) {

				try {
					$_SESSION = $this->plugin('cache')->get('session-'.autoCookie(SESSION_NAME));
				}
				catch (CacheException $e) {
					$_SESSION = null;
				}

			}

			if ($_SESSION === null) {

				enleveCookie(SESSION_NAME);
				poseCookie(SESSION_NAME, getToken(16), '+1 week', $params['subdomain']);

				$_SESSION = [];

			}

			define('SESSION_ID', autoCookie(SESSION_NAME));
			register_shutdown_function([$this, 'sessionStop']);

		}
		else {

			if (DOMAIN !== null) {
				ini_set('session.cookie_domain', $params['subdomain'] === true ? '.'.DOMAIN : HOST);
			}

			ini_set('session.name', SESSION_NAME);
			ini_set('session.cookie_lifetime', $params['expire']);
			ini_set('session.gc_maxlifetime', $params['expire']);
			ini_set('session.use_cookies', '1');
			ini_set('session.use_trans_sid', '0');
			ini_set('session.use_only_cookies', '1');

			session_start();
			define('SESSION_ID', session_id());

		}

	}

	public function sessionStop() {

		$params = ECP::$config['session'];

		if ($params) {
			$this->plugin('cache')->set('session-'.SESSION_ID, $_SESSION, $params['expire']);
		}

	}

	private function printHeader() {

		if ($this->ecpLoadAssets === true) {
			array_unshift($this->js, 'public/ecp/ecp.js');
		}

		if (Translation::isActive() === true) {
			array_push($this->js, 'public/js/i18n/'.Translation::getLanguage().'.js');
		}

		echo "<!DOCTYPE html>\n";
		echo "<html lang=\"".Translation::getLanguage()."\">\n";
		echo "<head>\n";
		echo $this->getMeta();
		echo $this->getCss();
		echo $this->getJavaScript();
		echo $this->getFavicon();
		echo $this->getTitre();
		echo $this->completeTete();
		echo "</head>\n";
		echo "<body>\n\n";
		echo "\t<script type=\"text/javascript\">\n";

		$config = [
			'ecp' => [
				'url' => URL,
				'host' => HOST,
				'debug' => DEBUG,
				'translation' => Translation::isActive() ? Translation::getLanguage() : null,
				'socket' => [
					'url' => SOCKET_URL,
				],
				'server' => [
					'date' => now(),
				],
			]
		] + Config::getShared();

		echo "\t\tConfig = ".json_encode($config, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE).";\n";

		foreach ($this->json as $variable => $valeur) {
			echo "\t\t$variable = ".json_encode($valeur, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE).";\n";
		}

		echo "\t</script>\n\n";
		ECP::$header = true;

	}

	private function profileBar() {

		$time = microtime(true) - MICRO_TIME;
		$time = $time * 1000;
		$time = number_format($time, 0, ',', ' ');

		$memory = memory_get_peak_usage() / 1024;
		$memory = number_format($memory, 0, ',', ' ');

		$query = ECP::hasBase() === true ? ECP::getBase()->countQuery() : 0;

		echo "\n\n\t<div id=\"ecp-profile\" style=\"position: absolute; right: 5px; top: 5px; padding: 4px; background-color: black; font-size: 11px; color: white; opacity: 0.5; border-radius: 5px; cursor: pointer;\">$time ms | $memory ko | $query qr</div>";

		if (ECP::hasBase() === true) {
			echo ECP::getBase()->showQuery();
		}

	}

	private function piedHtml() {
		echo "\n\n</body>\n";
		echo "</html>\n";
	}

	private function getMeta() {

		$meta = "\t<meta charset=\"utf-8\">\n";

		foreach ($this->meta as $name => $value) {
			$meta .= "\t<meta name=\"$name\" content=\"".htiti($value)."\">\n";
		}

		if ($this->touchIcon === true) {
			$meta .= "\t<link rel=\"apple-touch-icon\" href=\"".URL."public/touch-icon.png\">\n";
		}

		if ($this->touchStartup === true) {
			$meta .= "\t<link rel=\"apple-touch-startup-image\" href=\"".URL."public/touch-startup.png\">\n";
		}

		return $meta;

	}

	private function getJavaScript() {

		$js = '';

		foreach ($this->js as $file) {
			$js .= "\t<script ".src("$file?version=".$this->antiCache)."></script>\n";
		}

		return $js;

	}

	private function getCss() {

		$css = '';

		foreach ($this->css as $file) {

			if (mb_substr($file, 0, 4) !== 'http') {
				$file .= '?version='.$this->antiCache;
			}

			$css .= "\t<link rel=\"stylesheet\" ".href($file).">\n";

		}

		return $css;
	}

	private function getFavicon() {

		if ($this->favicon !== null) {
			return "\t<link rel=\"shortcut icon\" ".href($this->favicon).">\n";
		}
		else {
			return '';
		}

	}

	private function getTitre() {

		if ($this->titre !== null) {
			echo "\t<title>".htiti($this->titre)."</title>\n";
		}
		else {
			return '';
		}

	}

	protected function completeTete() {
		return '';
	}

}

/**
 * Table
 *
 * Parent des tables du projet.
 */
abstract class Table {

	public $engine = 'InnoDB';
	public $taille = 'mediumint';

	public $champ = [];
	public $unique = [];
	public $index = [];
	public $virtuel = [];
	public $onEvent = [];

	public $structure;
	public $lien;

	public function __construct() {

		if ($this->taille !== null) {

			$this->structure['id'] = [
				'type' => $this->taille,
				'option' => 'unsigned',
				'default' => null,
				'null' => false,
			];

		}

		foreach($this->champ as $nom => $structure) {

			$this->structure[ $nom ] = [
				'type' => $structure[0],
				'option' => isset($structure[1]) ? $structure[1] : null,
				'default' => isset($structure[2]) ? $structure[2] : null,
				'null' => isset($structure[3]) ? $structure[3] : false,
			];

		}

		foreach($this->virtuel as $nom => $params) {

			$this->lien[ $nom ] = [
				'table' => $params[0],
				'field' => $params[1],
				'where' => autoArray($params, 2),
			];

		}

	}

	public function onInsert($iElement) {
	}

	public function onUpdate($iElement) {
	}

	public function onDelete($iElement) {
	}

	protected function set($champ, $valeur) {
		$this->onEvent[ $champ ] = $valeur;
	}

}

/**
 * Package
 *
 * Parent des packages du projet.
 */
abstract class Package extends ECPCore {}

/**
 * Plugin
 *
 * Parent des plugins pour EcranPlat
 */
abstract class Plugin extends ECPCore {

	protected $nom = null;

	protected function table($table) {
		return ECP::getBase()->table($table, $this->nom);
	}

}

/**
 * Socket
 *
 * Parent des applications WebSocket du projet.
 */
abstract class Socket extends ECPCore {

	public function onAccept($client) {}
	public function onMessage($client, $json) {}
	public function onClose($client) {}
	public function onPeriod() {}

	protected function send($client, $json) {
		return WebSocket::push($client, $json);
	}

	protected function broadcast($json) {
		return WebSocket::broadcast($json);
	}

}

/**
 * ECP
 *
 * Fournit un ensemble de méthodes statiques internes à EcranPlat
 * Gère notamment :
 * - le chargement des files des pages
 * - la distribution des instances partagées
 * - la gestion des exceptions
 */
class ECP {

	public static $header = false;

	public static $config;

	private static $instances = [
		'base' => null,
		'proxyInfo' => null,
		'table' => [],
		'package' => [],
		'info' => [],
		'plugin' => [],
		'socket' => [],
	];

	public static function handleException($exception) {

		$type = $exception instanceof BaseException ? 'sql' : 'exception';
		$message = $exception->getMessage();

		ECP::makeMessage($type, $message, $exception->getFile(), $exception->getLine(), $exception->getTrace());
		exit;

	}

	public static function handleError($type, $message, $file, $ligne) {

		switch ($type) {

			case E_ERROR:
			case E_USER_ERROR:
				$type = 'exception';
				break;

			case E_WARNING:
			case E_USER_WARNING:
				$type = 'warning';
				break;

			case E_NOTICE:
			case E_USER_NOTICE:
				$type = 'notice';
				break;

			case E_STRICT:
				$type = 'strict';
				break;

			default:
				$type = 'inconnu';

		}

		$trace = debug_backtrace();

		ECP::makeMessage($type, $message, $file, $ligne, $trace);

		if ($type === 'exception') {
			exit;
		}

	}

	private static function makeMessage($type, $message, $file, $ligne, $trace) {

		if (defined('DEBUG') === false) {
			define('DEBUG', true);
		}

		switch ($type) {

			case 'exception':
				$titre = "Exception ultime !";
				$couleur = 'red';
				break;

			case 'warning':
			case 'notice':
				$titre = "Attention une erreur !";
				$couleur = 'orange';
				break;

			case 'strict':
				$titre = "Soyons strict !";
				$couleur = 'yellow';
				break;

			case 'sql':
				$titre = "Base de donnée mécontente !";
				$couleur = 'blue';
				break;

			default:
				$titre = "Qu'est ce donc que cela ?!";
				$couleur = 'green';

		}

		$file = "$file:$ligne";

		$traceTexte = '';
		foreach ($trace as $iTrace) {
			if (isset($iTrace['file']) === true) {
				$traceTexte .= mb_substr($iTrace['file'], mb_strlen(PATH)).':'.$iTrace['line'].' '.$iTrace['function']."()\n";
			}
		}

		$file = mb_substr($file, mb_strlen(PATH));

		$texte = "\n";
		$texte .= "_________________________________________________________\n";
		$texte .= "---------------------------------------------------------\n";
		$texte .= "\t $titre\n $message\n $file\n\n";
		$texte .= "--- Trace :\n $traceTexte";
		$texte .= "_________________________________________________________\n";

		if (CLI === true) {
			echo $texte;
		}
		else if (DEBUG === true and XHR === true) {
			echo nl2br($texte);
		}
		else if (DEBUG === true) {

			if (ECP::$header === false) {

				echo '<html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8" /></head><body>';
				ECP::$header = true;

			}

			echo '
			<div style="border: 3px solid '.$couleur.'; background-color: white; margin: 2px; padding: 10px; font-weight: bold; font-family: Verdana, Sans-Serif; font-size: 12px; color: black;">
				<div style="font-size: 18px;"> '.$titre.' </div>
				<div style="font-size: 14px; padding: 10px;"> '.nl2br($message).' </div>
				<div style="font-weight: normal;"> '.$file.' </div>
				<div style="padding: 15px 0px 5px 0px;"> Trace :</div>
				<div style="font-weight: normal;"> '.nl2br($traceTexte).' </div>
			</div>';
		}
		else if ($type === 'exception' or $type === 'sql') {
			echo "Erreur ultime !";
		}

		ECP::factory('plugin', 'error')->log($type, $file, $message, $traceTexte);

	}

	public static function loadConfig() {

		if (CLI === true) {
			CLI::creation();
		}

		try {
			$config = json_decode(file_get_contents(PATH.'config.json'), true);
		}
		catch (Throwable $exception) {
			throw new Exception("Please create a valid config.json file.");
		}

		$config['project']['debug'] = (bool) $config['project']['debug'];
		$config['project']['monitoring'] = (float) $config['project']['monitoring'];
		$config['sql']['debug'] = (bool) $config['sql']['debug'];
		$config['sql']['monitoring'] = (bool) $config['sql']['monitoring'];
		if ($config['session']) {
			$config['session']['memcached'] = (bool) $config['session']['memcached'];
			$config['session']['subdomain'] = (bool) $config['session']['subdomain'];
		}

		self::$config = $config;

		return $config;

	}

	public static function loadPage() {

		$page = ECP::getPageFromRequest();
		require($page);
		Translation::load($page);

	}

	private static function getPageFromRequest() {

		if (CLI === TRUE) {

			$page = isset($_SERVER['argv'][1]) ? trim($_SERVER['argv'][1]) : '';
			define('PAGE', $page);
			return PATH."server/commands/$page.php";

		}

		$requested = (string) getenv('REQUEST_URI');

		if (mb_strpos($requested, '?') !== false) {
			$requested = mb_substr($requested, 0, mb_strpos($requested, '?'));
		}

		$rootDirectory = mb_substr(URL, mb_strpos(URL, '/', 9));
		$requested = (string) mb_substr($requested, mb_strlen($rootDirectory));
		$page = $requested;

		if (mb_substr($page, -5) === '.html') {
			$page = mb_substr($page, 0, -5);
		}

		if ($page === '') {
			$page = 'index';
		}

		if (mb_substr($page, -1, 1) === '/') {
			$page .= 'index';
		}

		if (strpos($page, '..') !== false) {
			$page = '404';
		}

		$file = PATH."server/pages/$page.php";

		if (file_exists($file) === false) {

			$page = self::chercheAspirateur($page);
			define('POUSSIERE', mb_substr($requested, mb_strlen($page) -10));

		}

		define('PAGE', $page);
		return PATH."server/pages/$page.php";

	}

	public static function chercheAspirateur($page) {

		$explode = explode('/', $page);
		$explode = array_reverse($explode);

		if (count($explode) >= 10) {
			return '404';
		}

		$courrant = $page;

		foreach ($explode as $repertoire) {

			if (mb_substr($courrant, -1) === '/') {
				$repertoire .= '/';
			}

			$courrant = mb_substr($courrant, 0, -mb_strlen($repertoire));

			$file = PATH."server/pages/".$courrant."aspirateur.php";

			if (file_exists($file) === true) {
				return $courrant.'aspirateur';
			}

		}

		return '404';

	}

	public static function handleCLI() {

		CLI ? CLI::gestion() : define('CLI_ADMIN', false);

	}

	public static function hasMonitoring($page) {

		return $page and self::$config['project']['monitoring'] >= mt_rand(1, 1000) / 1000;

	}

	public static function getBase() {

		if (self::$instances['base'] === null) {

			require(PATH.'ecp/core/Base.php');
			self::$instances['base'] = new Base(self::$config['sql']);

			if (DEBUG === true or (CLI_ADMIN === false and MONITORING_SQL === true)) {
				self::$instances['base']->log = true;
			}

		}

		return self::$instances['base'];

	}

	public static function hasBase() {

		return self::$instances['base'] !== null;

	}

	public static function getInfo($info) {

		if (self::$instances['proxyInfo'] === null) {

			require_once(PATH.'ecp/core/Information.php');
			self::$instances['proxyInfo'] = new ProxyInfo();

		}

		if (in_array($info, self::$config['info']['cache']) === false) {
			return ECP::factory('info', $info);
		}

		self::$instances['proxyInfo']->information = $info;

		return self::$instances['proxyInfo'];

	}

	public static function factory($type, $nom, $plugin = null) {

		if (isset(self::$instances[$type][$nom]) === false) {

			if ($type === 'plugin') {
				$file = PATH."ecp/plugins/$nom/$nom.php";
			}
			else if ($plugin !== null) {
				$file = PATH."ecp/plugins/$plugin/$type/$nom.php";
			}
			else {
				$file = PATH.'server/'.$type."s/$nom.php";
			}

			$class = ucfirst($type);
			$chemin = explode('/', $nom);

			foreach ($chemin as $dossier) {
				$class .= ucfirst($dossier);
			}

			if ($type === 'package' or $type === 'info') {
				Translation::load($file);
			}

			self::require($file, $class);
			self::$instances[$type][$nom] = new $class();

		}

		return self::$instances[$type][$nom];

	}

	public static function require($file, $class) {

		if (DEBUG === true and file_exists($file) === false) {
			trigger_error("ECP can't open file '$file'", E_USER_ERROR);
		}

		require($file);

		if (DEBUG === true and class_exists($class) === false) {
			trigger_error("Please create a class '$class' in '$file'", E_USER_ERROR);
		}

	}

}


/* -------- Actions retournées par les méthodes prepare() des pages --------- */

abstract class Action {

	public $content = null;

	public function __construct($content = null) {
		$this->content = $content;
	}

}

class Redirection extends Action {

	public $permanent = false;

	public function __construct($content = null, $permanent = false) {
		$this->content = $content;
		$this->permanent = $permanent;
	}

}

class Json extends Action {

	public function __construct($content = []) {
		$this->content = $content;
	}

	public function add($cle, $valeur) {
		$this->content[ $cle ] = $valeur;
	}

}

class Png extends Action {}
class Jpeg extends Action {}
class Gif extends Action {}
class Webm extends Action {}
class Binary extends Action {}
class Page404 extends Action {}
class Stop extends Action {}
class Alert extends Action {}
class Failure extends Action {}
class Success extends Action {}


/* ------------------ Chargement automatique des classes -------------------- */

function __autoload($class) {

	if (mb_substr($class, 0, 6) === 'Parent') {

		$file = mb_strtolower(mb_substr($class, 6, 1)).mb_substr($class, 7);
		$file = PATH."server/parents/$file.php";

		ECP::require($file, $class);
		Translation::load($file);

	}
	else if (in_array($class, ['Information', 'BaseAdmin', 'CLI', 'Translation']) === true) {

		require(PATH."ecp/core/$class.php");

	}

}

class Raw {

	private $value;
	private $option;

	public function __construct($value, $option = null) {
		$this->value = $value;
		$this->option = $option;
	}

	public function get() {
		return $this->value;
	}

	public function option() {
		return $this->option;
	}

}

class Special extends Raw {}

class Config {

	private static $content = [];
	private static $shared = [];

	public static function prepare() {

		$path = PATH.'server/config/';

		foreach (scandir($path) as $file) {
			if (strpos($file, '.json')) {
				$name = mb_substr($file, 0, -5);
				$content = file_get_contents($path.$file);
				$content = json_decode($content, true);
				if ($content === null) {
					throw new Exception("Invalid json in $path$file");
				}
				self::$content[ $name ] = $content;
			}
		}

	}

	public static function get($name) {

		$actual = self::$content;
		foreach (explode('.', $name) as $level) {
			if (array_key_exists($level, $actual) === false) {
				throw new Exception("Can't find config $name");
			}
			$actual = $actual[ $level ];
		}
		return $actual;

	}

	public static function share() {
		self::$shared = func_get_args();
	}

	public static function getShared() {
		$shared = [];
		foreach (self::$shared as $name) {
			$shared[ $name ] = self::$content[ $name ];
		}
		return $shared;
	}

}

function config($name) {
	return Config::get($name);
}
