<?php

/**
 * [ Base ]
 *
 * EcranPlat's ORM
 * Allow acces to MySQL databases througth $this->table()
 */
class Base {

	/**
	 * Paramètres de connexion
	 */
	private $params = [];

	/**
	 * Propriétés internes
	 */
	private $connexion = null;
	private $ressource = null;
	private $queries = [];
	private $isAlive = true;

	/**
	 * Propriétés internes de l'ORM
	 */
	private $table;
	private $instance;
	private $structure;
	private $virtual;

	private $select;
	private $set;
	private $where;
	private $group;
	private $having;
	private $order;
	private $limit;

	private $cache;
	private $option;
	private $relation;
	private $cancelQuery;
	private $hasRelations;

	/**
	 * Informations publiques sur les requêtes
	 */
	public $lastId = null;
	public $affected = 0;

	public $log = false;
	public $time = 0;
	public $count = 0;

	/**
	 * Instanciation avec les paramètres de connexion au serveur SQL
	 */
	public function __construct($params) {

		if (
			isset($params['host']) === false or
			isset($params['user']) === false or
			isset($params['password']) === false or
			isset($params['database']) === false
		) {
			throw new Exception("Vos paramètres SQL ne sont pas valide.\nVeuillez vérifier votre fichier config.json.");
		}

		$this->params = $params;

	}

	public function table($table, $plugin = null) {

		$this->table = $table;
		$this->instance = ECP::factory('table', $table, $plugin);
		$this->structure = $this->instance->structure;
		$this->virtual = $this->instance->lien;

		$this->select = [];
		$this->set = [];
		$this->where = [];
		$this->group = null;
		$this->having = [];
		$this->order = null;
		$this->limit = null;

		$this->cache = null;
		$this->option = [];
		$this->relation = [];
		$this->cancelQuery = false;
		$this->hasRelations = false;

		return $this;

	}

	public function select($fields) {

		if (is_array($fields) === true) {

			$select = $fields;

		}
		else if (strpos($fields, ':') !== false) {

			$this->warning("Base->select('fieldA:fieldB') is deprecated.");
			$select = explode(':', $fields);

		}
		else {

			$select = func_get_args();

		}

		$this->select = array_merge($this->select, $select);

		return $this;

	}

	public function selectRaw($raw, $as) {

		$this->select[ $as ] = $raw;

		return $this;

	}

	public function selectForeign($table, $fields) {

		if (is_array($fields) === true) {

			$fields = $fields;

		}
		else {

			$fields = func_get_args();
			array_shift($fields);

		}

		$select[ $table ] = $fields;

		$this->select = array_merge($this->select, $select);

		return $this;

	}

	protected function analyseSelect($select) {

		$sql = '';
		$relations = [];

		if (count($select) === 0) {

			$select = array_keys($this->structure);

		}

		foreach ($select as $key => $field) {

			if (is_int($key) === true) {

				if ($field instanceof Raw) {

					$sql .= $field->get().' '.($field->option() === null ? '' : ' AS '.$field->option()).', ';

				}
				else {

					$sql .= "`$field`, ";

				}

			}
			else if (is_array($field) === true) {

				if (isset($this->structure[ $key ]) === true) {

					$sql .= "`$key`, ";

					$relation = [
						'destination' => $key,
						'table' => $this->structure[ $key ]['option'],
						'field' => 'id',
						'index' => $key,
						'select' => $field,
						'where' => null,
						'unique' => true,
					];

				}
				else if (isset($this->virtual[ $key ]) === true) {

					$relation = [
						'destination' => $key,
						'table' => $this->virtual[ $key ]['table'],
						'field' => $this->virtual[ $key ]['field'],
						'index' => 'id',
						'select' => $field,
						'where' => $this->virtual[ $key ]['where'],
						'unique' => true,
					];

				}
				else {

					throw new BaseException("Undefined field `$key` in `".$this->table."`.");

				}

				if (in_array($relation['field'], $relation['select']) === false) {

					array_unshift($relation['select'], $relation['field']);

				}

				$relations[] = $relation;

			}
			else {

				$sql .= "$field AS `$key`, ";

			}

		}

		$sql = substr($sql, 0, -2);

		return [
			'sql' => $sql,
			'relation' => $relations,
		];

	}

	public function set($field, $value) {

		$this->set[ $field ] = $value;

		return $this;

	}

	public function join() {

		return $this;

	}

	public function where($field, $mix = TOKEN, $value = TOKEN) {

		$this->whereHaving('WHERE', $field, $mix, $value);

		return $this;

	}

	public function whereNull($field) {

		$this->whereHaving('WHERE', $field, '=', null);

		return $this;

	}

	public function whereNotNull($field) {

		$this->whereHaving('WHERE', $field, '!=', null);

		return $this;

	}

	public function whereMultiple($list) {

		foreach ($list as $condition) {

			if (is_array($condition) === false) {
				$this->where($condition);
			}
			else if (count($condition) === 1) {
				$this->where($condition[0]);
			}
			else if (count($condition) === 2) {
				$this->where($condition[0], $condition[1]);
			}
			else if (count($condition) === 3) {
				$this->where($condition[0], $condition[1], $condition[2]);
			}
			else {
				throw new BaseException("Bad whereMultiple() call.");
			}

		}

		return $this;

	}

	public function in($rows, $index = 'id', $field = 'id') {

		return $this->inOrNot('IN', $rows, $index, $field);

	}

	public function notIn($rows, $index = 'id', $field = 'id') {

		return $this->inOrNot('NOT IN', $rows, $index, $field);

	}

	protected function inOrNot($inOrNot, $rows, $index, $field) {

		$ids = getIds($rows, $index);

		if (count($ids) === 0) {

			if ($inOrNot === 'IN') {

				$this->cancelQuery = true;

				$this->where[] = '1 = 0';

			}

			return $this;

		}

		$this->where[] = "`$field` $inOrNot(".implode(', ', $ids).")";

		return $this;

	}

	public function group() {

		$args = func_get_args();

		foreach ($args as &$arg) {

			$arg = $arg instanceof Raw ? $arg->get() : "`$arg`";

		}

		$this->group = implode(', ', $args);

		return $this;

	}

	public function having($field, $mix = TOKEN, $value = TOKEN) {

		$this->whereHaving('HAVING', $field, $mix, $value);

		return $this;

	}

	public function whereHaving($whereHaving, $field, $mix, $value) {

		if (empty($field) === true) {

			return ;

		}

		if ($mix === TOKEN) {

			$this->{strtolower($whereHaving)}[] = "($field)";

			return ;

		}

		if ($value === TOKEN) {

			$value = $mix;
			$comparaison = '=';

		}
		else if (in_array($mix, ['=', '!=', '<', '>', '<=', '>=', 'like', 'not like', 'in', 'not in']) === false) {

			throw new BaseException("'$mix' is not a valid operator.");

		}
		else {
			$comparaison = $mix;
		}

		if ($comparaison === 'in' or $comparaison === 'not in') {

			$value = "('".implode("', '", $value)."')";

		}
		else {

			$value = $this->encode($field, $value);

		}

		if ($value === 'NULL') {
			$comparaison = ($comparaison === '=') ? 'IS' : 'IS NOT';
		}

		$this->{strtolower($whereHaving)}[] = "`$field` ".strtoupper($comparaison)." $value";

	}

	public function order() {

		$nbOrder = func_num_args() / 2;

		if (is_int($nbOrder) === false) {
			throw new BaseException("Il faut donner une paire d'arguments à order(), comme order('id', 'desc').");
		}

		$sql = '';

		for ($i = 0; $i <= $nbOrder; $i += 2) {

			$sens = mb_strtoupper(func_get_arg($i + 1));

			if ($sens !== 'ASC' and $sens !== 'DESC') {
				throw new BaseException("ORDER BY '$sens' n'est pas valide. Veuillez utiliser 'ASC' ou 'DESC'.");
			}

			$field = func_get_arg($i);
			$field = $field instanceof Raw ? $field->get() : "`$field`";

			$sql .= "$field $sens, ";

		}

		$this->order = substr($sql, 0, -2);

		return $this;

	}

	public function limit($offset, $row = null) {

		$offset = (int) $offset;

		if ($row === null) {

			$this->limit = "$offset";

		}
		else {

			$row = (int) $row;

			$this->limit = "$offset, $row";

		}

		return $this;

	}

	public function cache($key, $duree = 60) {

		$this->cache = ['cle' => $key, 'duree' => $duree];

		return $this;

	}

	public function option($option) {

		$option = strtoupper($option);

		if (in_array($option, ['IGNORE', 'REPLACE', 'NO-TRIGGER', 'BIG-DATA', 'SET-INFO']) === false) {

			throw new BaseException("'$option' n'est pas une option valide.");

		}

		$this->option[ $option ] = true;

		return $this;

	}

	public function get($index = null) {

		if ($this->cache !== null) {

			try {
				return ECP::factory('plugin', 'cache')->get($this->cache['cle']);
			}
			catch (CacheException $e) {
			}

		}

		if ($this->cancelQuery === true) {
			return $index === false ? null : [];
		}

		if ($this->hasRelations === true) {

			if ($index !== null and $index !== 'id') {
				throw new BaseException("Impossible d'utiliser l'index '$index' car vous faîtes une relation.");
			}

			$index = 'id';

		}

		if ($index === false) {
			$this->warning("Base::get(false) is deprecated.");
		}

		$analyse = $this->analyseSelect($this->select);

		$sql = 'SELECT '.$analyse['sql'].' FROM `'.$this->table.'`';

		$sql .= $this->getWol();

		$this->doQuery($sql);

		$rows = $this->getRow($index);

		foreach (array_merge($analyse['relation'], $this->relation) as $relation) {

			$this->executeRelation($rows, $relation);

		}

		if ($this->cache !== null) {

			ECP::factory('plugin', 'cache')->set($this->cache['cle'], $rows, $this->cache['duree']);

		}

		return $rows;

	}

	public function first() {

		$this->limit(0, 1);

		return autoArray($this->get(), 0);

	}

	public function value($index = null) {

		$row = $this->first();

		if ($row === null) {
			return null;
		}

		return $index ? autoArray($row, $index) : first($row);

	}

	/**
	 * @deprecated
	 * use option BIG-DATA instead
	 */
	public function getCurseur() {

		$analyse = $this->analyseSelect($this->select);

		$sql = 'SELECT '.$analyse['sql'].' FROM `'.$this->table.'`';

		$sql .= $this->getWol();

		$this->doQuery($sql);

		return $this->ressource;

	}

	public function getSuivant(&$ressource, $quantite = null) {

		$this->ressource = $ressource;

		if (is_object($ressource) === false) {

			return false;

		}

		if ($quantite === null) {

			$row = $ressource->fetch_assoc();

			if ($row === null) {

				$ressource->free();
				$ressource = null;

				return false;

			}

			return $this->decodeRow($row);

		}

		$rows = [];
		$compteur = 0;

		while($compteur++ < $quantite and $row = $ressource->fetch_assoc()) {

			$rows[ $row['id'] ] = $this->decodeRow($row);

		}

		if ($row === null) {

			$ressource->free();
			$ressource = null;

		}

		if (count($rows) === 0) {

			return false;

		}

		return $rows;

	}

	public function count() {

		$this->select = ['count' => 'COUNT(*)'];

		return (int) $this->value();

	}

	public function has() {

		$this->select = ['id'];
		$this->limit(0, 1);

		return count($this->get()) > 0;

	}

	public function insert(&$row) {

		if (isset($this->option['NO-TRIGGER']) === false) {

			$row += $this->onEvent('insert', $row);

		}

		$encoded = $this->encodeRow($row);

		$option = isset($this->option['IGNORE']) === true ? ' IGNORE' : '';
		$action = isset($this->option['REPLACE']) === true ? ' REPLACE' : 'INSERT';

		$field = '`'.implode('`, `', array_keys($encoded)).'`';

		$sql = "$action $option INTO `".$this->table."` ($field) VALUES (".implode(", ", $encoded).")";

		$this->doQuery($sql);

		if ($this->affected > 0) {

			$row['id'] = $this->insertId;

		}

		return $this->affected;

	}

	public function insertMultiple($rows) {

		if (count($rows) === 0) {
			return 0;
		}

		$values = [];

		foreach ($rows as $row) {

			if (isset($this->option['NO-TRIGGER']) === false) {

				$row += $this->onEvent('insert', $row);

			}

			$encoded = $this->encodeRow($row);

			$values[] = '('.implode(', ', $encoded).')';

		}

		$option = isset($this->option['IGNORE']) === true ? ' IGNORE' : '';

		$field = '`'.implode('`, `', array_keys($encoded)).'`';

		$sql = "INSERT $option INTO `".$this->table."` ($field) VALUES ".implode(', ', $values);

		$this->doQuery($sql);

		return $this->affected;

	}

	public function update(&$row = TOKEN) {

		if ($this->cancelQuery === true) {
			return 0;
		}

		if (isset($this->option['NO-TRIGGER']) === false and $row !== TOKEN) {

			$this->set += $this->onEvent('update', $row);

		}

		$set = '';

		foreach ($this->set as $field => $value) {

			if ($row !== TOKEN) {
				$row[ $field ] = $value;
			}

			$value = $this->encode($field, $value);

			$set .= "`$field` = $value, ";

		}

		$set = substr($set, 0, -2);

		$option = isset($this->option['IGNORE']) === true ? ' IGNORE' : '';

		$sql = "UPDATE $option `".$this->table."` SET $set";

		if ($row !== TOKEN) {

			if (isset($row['id']) === false or is_int($row['id']) === false) {
				throw new Exception("delete() argument must have an id (integer).");
			}

			$this->where('id', $row);

		}

		$sql .= $this->getWol();

		$this->doQuery($sql);

		return $this->affected;

	}

	public function delete($row = TOKEN) {

		if ($this->cancelQuery === true) {
			return 0;
		}

		$sql = 'DELETE FROM `'.$this->table.'`';

		if ($row !== TOKEN) {

			if (isset($row['id']) === false or is_int($row['id']) === false) {
				throw new Exception("delete() argument must have an id (integer).");
			}

			if (isset($this->option['NO-TRIGGER']) === false) {

				$this->onEvent('delete', $row);

			}

			$this->where('id', $row);

		}

		$sql .= $this->getWol();

		$this->doQuery($sql);

		return $this->affected;

	}

	public function relation($destination, $table, $field, $select, $where = null) {

		$this->relation[] = [
			'destination' => $destination,
			'table' => $table,
			'field' => $field,
			'index' => 'id',
			'select' => $select,
			'where' => $where,
			'unique' => false,
		];

		$this->hasRelations = true;

		return $this;

	}

	public function query($sql, $index = null, $curseur = false) {

		$this->doQuery($sql);

		if (is_object($this->ressource) === false or $curseur === true) {
			return $this->ressource;
		}

		$rows = $this->getRow($index);

		return $index === false ? autoArray($rows, 0) : $rows;

	}

	protected function getWol() {

		$sql = '';

		if ($this->where !== []) {
			$sql .= ' WHERE '.implode(' AND ', $this->where);
		}

		if ($this->group !== null) {
			$sql .= ' GROUP BY '.$this->group;
		}

		if ($this->having !== []) {
			$sql .= ' HAVING '.implode(' AND ', $this->having);
		}

		if ($this->order !== null) {
			$sql .= ' ORDER BY '.$this->order;
		}

		if ($this->limit !== null) {
			$sql .= ' LIMIT '.$this->limit;
		}

		return $sql;

	}

	/**
	 * Echape une donnée pour l'utiliser dans une requête
	 *
	 * @param mix value
	 * @return mix - value échappée
	 */
	public function escape($value) {

		if ($value === null) {
			return 'NULL';
		}

		$this->connect();

		$value = $this->connexion->real_escape_string($value);

		return "'$value'";

	}

	public function echape($value) {

		return $this->escape($value);

	}

	protected function encodeRow($row) {

		$iReel = [];

		foreach ($row as $field => $value) {

			if (isset($this->structure[ $field ]) === true) {

				$iReel[ $field ] = $this->encode($field, $value);

			}

		}

		return $iReel;

	}

	public function encode($field, $value) {

		if ($value === null) {
			return 'NULL';
		}

		if ($value instanceof Raw) {
			return $value->get();
		}

		if ($field === 'id') {
			return (int) getId($value);
		}

		if (isset($this->structure[ $field ]) === false) {
			throw new BaseException("Le field $field n'existe pas dans la table ".$this->table.".");
		}

		$structure = $this->structure[ $field ];

		switch ($structure['type']) {

			case 'bool':
				return $value === true ? 1 : 0;

			case 'tinyint':
			case 'smallint':
			case 'mediumint':
			case 'int':
			case 'bigint':
				return (int) $value;

			case 'float':
			case 'decimal':
			case 'double':
				return (float) $value;

			case 'char':
			case 'varchar':
				if (mb_strlen($value) > $structure['option']) {
					$value = mb_substr($value, 0, $structure['option']);
				}
				return $this->escape($value);
			case 'tinytext':
			case 'text':
			case 'mediumtext':
			case 'longtext':
			case 'tinyblob':
			case 'blob':
			case 'mediumblob':
			case 'longblob':
				return $this->escape($value);

			case 'date':
			case 'datetime':
			case 'time':
			case 'year':
				return $this->escape($value);

			case 'enum':
				return $this->escape($value);

			case 'binary':
				return $this->escape($value);

			case 'json':
				return $this->escape(json_encode($value, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));

			case 'table':
			case 'info':
				return (int) getId($value);

			default:
				throw new BaseException("Type '".$structure['type']."' inconnu (field $field).");

		}

	}

	protected function decodeRow($row) {

		$position = 0;

		foreach ($row as $field => &$value) {

			$value = $this->decode($field, $value, $position++);

		}

		return $row;

	}

	protected function decode($field, $value, $position) {

		if ($value === null) {
			return null;
		}

		if (isset($this->structure[ $field ]) === false) {

			$meta = $this->ressource->fetch_field_direct($position);

			switch ($meta->type) {

				case MYSQLI_TYPE_TINY:
				case MYSQLI_TYPE_SHORT:
				case MYSQLI_TYPE_LONG:
				case MYSQLI_TYPE_LONGLONG:
				case MYSQLI_TYPE_INT24:
					return (int) $value;

				case MYSQLI_TYPE_FLOAT:
				case MYSQLI_TYPE_DOUBLE:
				case MYSQLI_TYPE_DECIMAL:
				case MYSQLI_TYPE_NEWDECIMAL:
					return (float) $value;

				default:
					return $value;

			}

		}

		$structure = $this->structure[ $field ];

		switch ($structure['type']) {

			case 'bool':
				return $value === '1' ? true : false;

			case 'tinyint':
			case 'smallint':
			case 'mediumint':
			case 'int':
			case 'bigint':
				return (int) $value;

			case 'float':
			case 'decimal':
			case 'double':
				return (float) $value;

			case 'char':
			case 'varchar':
			case 'tinytext':
			case 'text':
			case 'mediumtext':
			case 'longtext':
			case 'tinyblob':
			case 'blob':
			case 'mediumblob':
			case 'longblob':
				return $value;

			case 'date':
			case 'datetime':
			case 'time':
			case 'year':
				return $value;

			case 'enum':
				return $value;

			case 'binary':
				return $value;

			case 'json':
				return json_decode($value, true);

			case 'table':
				return ['id' => (int) $value];

			case 'info':
				if (isset($this->option['SET-INFO']) === true) {
					return ECP::getInfo($structure['option'])->get((int) $value);
				}
				return ['id' => (int) $value];

			default:
				throw new BaseException("Type '".$structure['type']."' inconnu (field $field).");

		}

	}

	/**
	 * [ PRIVATE ]
	 * Ce connect si besoin au serveur SQL
	 */
	protected function connect() {

		if ($this->connexion === false) {

			throw new BaseException("Requête impossible, la connexion à MySQL est fermée après la méthode 'prepare'");

		}

		if (is_object($this->connexion) === true) {

			return true;

		}

		$this->connexion = new mysqli($this->params['host'], $this->params['user'], $this->params['password'], $this->params['database']);

		if ($this->connexion->connect_error !== null) {

			$error = $this->connexion->connect_error;

			$this->isAlive = false;
			$this->connexion = false;

			throw new BaseException("Connexion impossible : $error");

		}

		$this->connexion->set_charset('utf8mb4');

	}

	public function isAlive() {

		if ($this->isAlive === false) {
			return false;
		}

		if (is_object($this->connexion) === true) {
			return true;
		}

		$this->restart();
		$this->connect();

		return is_object($this->connexion) === true;

	}

	/**
	 * Coupe la connexion au serveur SQL
	 */
	public function stop() {

		if (is_object($this->connexion) === true) {

			$this->connexion->close();

		}

		$this->connexion = false;

	}

	/**
	 * Ré autorise les connexions SQL (ne devrait jamais être utilisé)
	 */
	public function restart() {

		$this->stop();

		$this->connexion = null;

	}

	protected function executeRelation(&$rows, $relation) {

		if (count($rows) === 0) {
			return ;
		}

		$this->microTable($relation['table']);

		$ids = getIds($rows, $relation['index']);

		$analyse = $this->analyseSelect($relation['select']);

		if (count($ids) > 0) {

			$sql = 'SELECT '.$analyse['sql'].' FROM `'.$relation['table'].'` WHERE `'.$relation['field'].'` IN('.implode(', ', $ids).')';

			if ($relation['where'] !== null) {

				$sql .= ' AND '.$relation['where'];

			}

			$index = $relation['unique'] === true ? $relation['field'] : null;

			$igRelation = $this->query($sql, $index);

		}
		else {

			$igRelation = [];

		}

		foreach ($analyse['relation'] as $sousRelation) {

			$this->executeRelation($igRelation, $sousRelation);

		}

		if ($relation['unique'] === true) {

			foreach ($rows as &$row) {

				$id = getId($row[ $relation['index'] ]);

				$row[ $relation['destination'] ] = autoArray($igRelation, $id);

			}

		}
		else {

			foreach ($rows as &$row) {

				$row[ $relation['destination'] ] = [];

			}

			foreach ($igRelation as $iRelation) {

				$id = getId($iRelation[ $relation['field'] ]);

				$rows[ $id ][ $relation['destination'] ][] = $iRelation;

			}

		}

	}

	protected function microTable($table) {

		$this->table = $table;
		$this->instance = ECP::factory('table', $table);
		$this->structure = $this->instance->structure;
		$this->virtual = $this->instance->lien;

		return $this;

	}

	protected function warning($text) {

		$state = [];

		foreach (['table', 'select', 'set', 'where', 'group', 'having', 'order', 'limit', 'cache', 'option', 'relation', 'cancelQuery', 'hasRelations', 'ressource'] as $field) {
			$state[ $field ] = $this->{$field};
		}

		trigger_error($text);

		$this->microTable($state['table']);

		foreach (['select', 'set', 'where', 'group', 'having', 'order', 'limit', 'cache', 'option', 'relation', 'cancelQuery', 'hasRelations', 'ressource'] as $field) {
			$this->{$field} = $state[ $field ];
		}

	}

	protected function doQuery($query, $isRecursive = false) {

		$this->connect();

		$startTime = microtime(true);

		$this->ressource = $this->connexion->query($query);

		if ($this->log === true) {

			$this->queries[] = ['query' => $query, 'time' => microtime(true) - $startTime, 'table' => $this->table];

		}

		$this->time += microtime(true) - $startTime;
		$this->count++;
		$this->insertId = $this->connexion->insert_id;
		$this->affected = $this->connexion->affected_rows;

		if ($this->ressource === false) {

			$codeErreur = $this->connexion->errno;

			if ($codeErreur === 1146 and $isRecursive === false) {

				BaseAdmin::create($this->table);

				return $this->doQuery($query, true);

			}

			if ($codeErreur === 2006 and $isRecursive === false) {

				$this->restart();

				return $this->doQuery($query, true);

			}

			throw new BaseException("Erreur MySQL n°$codeErreur : ".$this->connexion->error."\n\n $query", $codeErreur);

		}

	}

	protected function onEvent($event, $row) {

		$this->instance->onEvent = [];

		$methode = [
			'insert' => 'onInsert',
			'update' => 'onUpdate',
			'delete' => 'onDelete',
		];

		$methode = $methode[ $event ];

		$this->instance->$methode($row);

		return $this->instance->onEvent;

	}

	protected function getRow($index) {

		$resultat = [];
		$controle = 0;

		while ($row = $this->ressource->fetch_assoc()) {

			$row = $this->decodeRow($row);

			if (is_string($index) === true) {

				$resultat[ getId($row[ $index ]) ] = $row;

			}
			else {

				$resultat[] = $row;

			}

			if ($controle++ === 1000 and isset($this->option['BIG-DATA']) === false) {

				$this->warning("Query return more than 1000 lines, you should use getCurseur()");

			}

		}

		$this->ressource->free();

		return $resultat;

	}

	/**
	 * [ Infos ]
	 * Get field's structure infos
	 *
	 * @return array - infos
	 */
	public function field($name) {

		return autoArray($this->structure, $name);

	}

	/**
	 * [ Statistiques ]
	 * Compte le nombre requêtes faites (le log doit être activé)
	 *
	 * @return int - nombre de requêtes
	 */
	public function countQuery() {

		return count($this->queries);

	}

	/**
	 * [ Statistiques ]
	 * Retourne les requêtes faites (le log doit être activé)
	 *
	 * @return array - liste de requêtes
	 */
	public function getQuery() {

		return $this->queries;

	}

	/**
	 * [ Statistiques ]
	 * Affiche les requêtes faites (le log doit être activé)
	 *
	 * @return string HTML - tableau des requêtes
	 */
	public function showQuery() {

		if ($this->log === false) {
			throw new BaseException("Base::showQuery() required sql.monitoring in config.json");
		}

		$html = '
		<div id="ecp-profile-sql" style="display: none; position: absolute; right: 5px; top: 30px; color: white; opacity: 0.6; font-size: 12px;">';

		foreach($this->queries as $query) {

			$query['query'] = htiti($query['query']);
			$query['query'] = preg_replace('/(SELECT|UPDATE|INSERT|DELETE|FROM|WHERE)\s/', '<strong>$1</strong> ', $query['query']);

			$html .= '
			<div style="background-color: black; margin: 2px; padding: 3px; border-radius: 3px;">'.$this->getTime($query['time']).' '.$query['query'].'</div>';

		}

		$count = count($this->queries);
		$count = max(1, $count);

		$html .= '
			<div style="background-color: black; margin: 2px; padding: 3px; border-radius: 3px;">'.$this->getTime($this->time).' '.$count.' requests, average time: '.$this->getTime($this->time / $count).'</div>
		</div>';

		return $html;

	}

	/**
	 * [ PRIVATE ]
	 * Retourne un temps d'execution en ms
	 */
	protected function getTime($time) {

		return '<span style="color: '.$this->getCouleur($time).';">'.number_format($time * 1000, 2, '.', ' ').' ms</span>';

	}

	/**
	 * [ PRIVATE ]
	 * Retourne un code de couleur selon les performances
	 */
	protected function getCouleur($time) {

		$time = floatval($time) * 1000;

		if ($time < 1) {
			return 'green';
		}
		if ($time < 10) {
			return 'orange';
		}
		return 'red';

	}

}

class BaseException extends Exception {}
