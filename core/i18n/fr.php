<?php

/**
 * Convertit un nombre de seconde en texte comme '3 minutes'
 * Peut aussi prendre une date au fomrat Y:m:d H:i:s en entrée
 *
 * @param int $duree (en seconde)
 * @return string
 */
function dureeTexte($duree) {

	if (is_numeric($duree) === false) {
		$duree = time() - getTime($duree);
	}

	$duree = abs($duree);

	// Secondes :
	if ($duree < 60) {
		return $duree.' seconde'.s($duree);
	}

	// Minutes :
	$duree /= 60;

	if ($duree < 60) {
		return floor($duree).' minute'.s($duree);
	}

	// Heures :
	$duree /= 60;

	if ($duree < 24) {
		return floor($duree).' heure'.s($duree);
	}

	// Jours :
	$duree /= 24;

	if ($duree < 30) {
		return floor($duree).' jour'.s($duree);
	}

	// Mois :
	$duree /= 30;

	if ($duree < 12) {
		return floor($duree).' mois';
	}

	// Années :
	$duree /= 12;

	return floor($duree).' an'.s($duree);

}

/**
 * Convertit une date au format Y:m:d H:i:s en une date plus lisible
 * Ex : 2009-01-01 01:05:45 -> Le 1 janvier, à 1:05 | Jeudi, à 1:05 | Hier, à 1:05 | Il y a 1 heure | Il y a 5 minutes
 *
 * @param string $date
 * @return string
 */
function dateTexte($date) {

	if ($date === null) {
		return 'Jamais';
	}

	$ecart = time() - getTime($date);

	// Il y a moins de 4 heures
	if ($ecart > 0 and $ecart < 3600 * 4) {
		return 'Il y a '.dureeTexte($ecart);
	}

	// Dans les 10 minutes
	if ($ecart < 0 and $ecart > -60 * 10) {
		return 'Dans '.dureeTexte($ecart);
	}

	$heure = substr($date, 11, 2);
	$minute = substr($date, 14, 2);

	// Aujourd'hui
	if (substr($date, 0, 10) === date('Y-m-d')) {
		return "Aujourd'hui, à $heure:$minute";
	}

	// Hier
	if (substr($date, 0, 10) === date('Y-m-d', strtotime('-1 day'))) {
		return "Hier, à $heure:$minute";
	}

	// Demain
	if (substr($date, 0, 10) === date('Y-m-d', strtotime('+1 day'))) {
		return "Demain, à $heure:$minute";
	}

	$listeJour = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

	// Dans les 5 jours
	if (abs($ecart) < 3600 * 24 * 5) {
		return $listeJour[ date('w', getTime($date)) ].", à $heure:$minute";
	}

	$listeMois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
	$mois = $listeMois[ substr($date, 5, 2) - 1 ];
	$jour = (int) substr($date, 8, 2);

	// Futur (avec heure)
	if ($ecart < 0) {
		return "Le $jour $mois à $heure:$minute";
	}

	// Dans les 2 mois
	if ($ecart < 3600 * 24 * 60) {
		return "Le $jour $mois";
	}

	$annee = substr($date, 0, 4);

	// Il y a fort fort longtemps
	return "En $mois $annee";

}

/**
 * Convertit une date au format Y:m:d [H:i:s] en une date plus lisible à un jour près
 * Ex : 2009-01-01 -> Le 1 janvier | Jeudi | Hier | Aujourd'hui
 *
 * @param string $date
 * @return string
 */
function jourTexte($date) {

	// Aujourd'hui
	if (substr($date, 0, 10) === date('Y-m-d')) {
		return "Aujourd'hui";
	}

	// Hier
	if (substr($date, 0, 10) === date('Y-m-d', strtotime('-1 day'))) {
		return "Hier";
	}

	$ecart = time() - getTime($date);
	$listeJour = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

	// Dans les 5 derniers jours
	if (abs($ecart) < 3600 * 24 * 5) {
		return $listeJour[ date('w', getTime($date)) ];
	}

	$listeMois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
	$mois = $listeMois[ substr($date, 5, 2) - 1 ];
	$jour = (int) substr($date, 8, 2);

	// Cette année
	if (substr($date, 0, 4) === date('Y')) {
		return "Le $jour $mois";
	}

	$annee = substr($date, 0, 4);

	// Il y a fort fort longtemps
	return "Le $jour $mois $annee";

}

function heure() {

	return date('H').'h '.date('i');

}

function dateLocale($format, $time = null) {

	if ($time === null) {
		$time = time();
	}

	return date($format, $time);

}

