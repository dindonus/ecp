<?php

/**
 * Convertit un nombre de seconde en texte comme '3 minutes'
 * Peut aussi prendre une date au fomrat Y:m:d H:i:s en entrée
 *
 * @param int $duree (en seconde)
 * @return string
 */
function dureeTexte($duree) {

	if (is_numeric($duree) === false) {
		$duree = time() - getTime($duree);
	}

	$duree = abs($duree);

	// Secondes :
	if ($duree < 60) {
		return $duree.' seconde'.s($duree);
	}

	// Minutes :
	$duree /= 60;

	if ($duree < 60) {
		return floor($duree).' minute'.s($duree);
	}

	// Heures :
	$duree /= 60;

	if ($duree < 24) {
		return floor($duree).' hour'.s($duree);
	}

	// Jours :
	$duree /= 24;

	if ($duree < 30) {
		return floor($duree).' day'.s($duree);
	}

	// Mois :
	$duree /= 30;

	if ($duree < 12) {
		return floor($duree).' month'.s($duree);
	}

	// Années :
	$duree /= 12;

	return floor($duree).' year'.s($duree);

}

/**
 * Convertit une date au format Y:m:d H:i:s en une date plus lisible
 * Ex : 2009-01-01 01:05:45 -> Le 1 janvier, à 1:05 | Jeudi, à 1:05 | Hier, à 1:05 | Il y a 1 heure | Il y a 5 minutes
 *
 * @param string $date
 * @return string
 */
function dateTexte($date) {

	if ($date === null) {
		return 'Never';
	}

	$ecart = time() - getTime($date);

	// Il y a moins de 4 heures
	if ($ecart > 0 and $ecart < 3600 * 4) {
		return dureeTexte($ecart).' ago';
	}

	// Dans les 10 minutes
	if ($ecart < 0 and $ecart > -60 * 10) {
		return 'In '.dureeTexte($ecart);
	}

	$heure = heure($date);

	$dateLocale = decaleDate($date, '-7 hours');

	// Aujourd'hui
	if (substr($dateLocale, 0, 10) === dateLocale('Y-m-d')) {
		return "Today, at $heure";
	}

	// Hier
	if (substr($dateLocale, 0, 10) === dateLocale('Y-m-d', strtotime('-1 day'))) {
		return "Yesterday, at $heure";
	}

	// Demain
	if (substr($dateLocale, 0, 10) === dateLocale('Y-m-d', strtotime('+1 day'))) {
		return "Tomorow, at $heure";
	}

	$listeJour = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

	// Dans les 5 jours
	if (abs($ecart) < 3600 * 24 * 5) {
		return $listeJour[ dateLocale('w', getTime($date)) ].", at $heure";
	}

	$listeMois = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	$mois = $listeMois[ substr($dateLocale, 5, 2) - 1 ];
	$jour = (int) substr($dateLocale, 8, 2);

	// Futur (avec heure)
	if ($ecart < 0) {
		return "$mois $jour, at $heure";
	}

	// Dans les 2 mois
	if ($ecart < 3600 * 24 * 60) {
		return "$mois $jour";
	}

	$annee = substr($dateLocale, 0, 4);

	// Il y a fort fort longtemps
	return "$mois $annee";

}

/**
 * Convertit une date au format Y:m:d [H:i:s] en une date plus lisible à un jour près
 * Ex : 2009-01-01 -> Le 1 janvier | Jeudi | Hier | Aujourd'hui
 *
 * @param string $date
 * @return string
 */
function jourTexte($date) {

	$dateLocale = decaleDate($date, '-7 hours');

	// Aujourd'hui
	if (substr($dateLocale, 0, 10) === dateLocale('Y-m-d')) {
		return "Today";
	}

	// Hier
	if (substr($dateLocale, 0, 10) === dateLocale('Y-m-d', strtotime('-1 day'))) {
		return "Yesterday";
	}

	$ecart = time() - getTime($date);
	$listeJour = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

	// Dans les 5 derniers jours
	if (abs($ecart) < 3600 * 24 * 5) {
		return $listeJour[ dateLocale('w', getTime($date)) ];
	}

	$listeMois = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	$mois = $listeMois[ substr($dateLocale, 5, 2) - 1 ];
	$jour = (int) substr($dateLocale, 8, 2);

	// Cette année
	if (substr($dateLocale, 0, 4) === dateLocale('Y')) {
		return "$mois $jour";
	}

	$annee = substr($dateLocale, 0, 4);

	// Il y a fort fort longtemps
	return "$mois $jour $annee";

}

function heure($date = null) {

	if ($date === null) {
		$date = now();
	}

	return dateLocale('g:ia', getTime($date));

}

function dateLocale($format, $time = null) {

	if ($time === null) {
		$time = time();
	}

	return date($format, strtotime('-7 hours', $time));

}

