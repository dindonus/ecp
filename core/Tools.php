<?php

/**
 * [ Tools ]
 *
 * Divers outils pour gérer les projets.
 * Permet de détecter les parses errors.
 */

class Tools {

	public static function parse() {

		$fichiers = self::scanFichier();

		$erreur = self::doParse($fichiers);

		echo "$erreur parses errors détectées\n";

	}


	protected static function scanFichier() {

		echo "scan du projet en cours...\n";

		$fichiers = [];
		exec('find '.PATH.' -type f | grep ".php" | grep -v ".git"', $fichiers);

		echo "> ".count($fichiers)." fichiers php détectés\n";

		return $fichiers;

	}

	protected static function doParse($fichiers) {

		echo "parsing en cours...\n";

		$avancement = 0;
		$erreur = 0;

		foreach ($fichiers as $fichier) {

			$resultat = exec("php -l $fichier");

			if (mb_substr($resultat, 0, 25) !== "No syntax errors detected") {

				$erreur++;

			}

			echo "\r> ".round($avancement++ / count($fichiers) * 100)." %";

		}

		echo "\r> terminé\n";

		return $erreur;

	}

}
