<?php

/**
 * [ TranslationAdmin ]
 *
 * Gestion de l'internationalisation (i18n)
 * Permet la compilation, l'export et l'import des traductions du projet.
 */

class TranslationAdmin {

	protected static $autoIncrement = 0;

	public static function compile() {

		$fichiers = self::scanFichier('__(');

		$increment = self::scanIncrement($fichiers);

		self::doCompile($fichiers);
		self::doFactorize();

		echo "nouvelles traductions : ".(self::$autoIncrement - $increment)."\n";
		echo "auto incrément actuel : ".self::$autoIncrement."\n";

	}

	public static function export() {

		$fichiers = self::scanFichier('___(');

		self::doExport($fichiers);

		echo "export terminé\n";

	}

	public static function import() {

		$fichiers = self::scanFichier('___(');

		$optimiseur = self::getOptimiseur($fichiers);

		self::doImport($optimiseur);

	}

	public static function clean() {

		$fichiers = self::scanFichier('___(');

		$optimiseur = self::getOptimiseur($fichiers);

		self::doClean($optimiseur);

	}

	protected static function scanFichier($cherche) {

		echo "scan du projet en cours...\n";

		$fichiers = [];
		exec('grep -Irl --include=*.php --include=*.js --exclude-dir=".svn" "'.$cherche.'" '.PATH, $fichiers);

		echo "> ".count($fichiers)." fichiers détectés\n";

		return $fichiers;

	}

	protected static function scanIncrement($fichiers) {

		echo "calcul de l'auto incrément...\n";

		foreach ($fichiers as $fichier) {

			$contenu = file_get_contents($fichier);

			preg_replace_callback('/___\((.*),/sU', 'TranslationAdmin::cbScanIncrement', $contenu);

		}

		echo "> ".self::$autoIncrement."\n";

		return self::$autoIncrement;

	}

	protected static function doCompile($fichiers) {

		echo "compilation en cours...\n";

		$avancement = 0;

		foreach ($fichiers as $fichier) {

			$contenu = file_get_contents($fichier);

			$contenu = preg_replace_callback('/([^_])__\("/sU', 'TranslationAdmin::cbCompile', $contenu);

			file_put_contents($fichier, $contenu);

			echo "\r> ".round( ($avancement++ / count($fichiers)) * 100)." %";

		}

		echo "\r> terminé\n";

	}

	protected static function doExport($fichiers) {

		echo "export en cours...\n";

		$export = [];

		$avancement = 0;

		foreach ($fichiers as $fichier) {

			$dictionary = self::getDictionnaire($fichier);

			$traduction = self::getTranslation($fichier);

			foreach ($traduction as $id => $texte) {

				if (isset($dictionary[ $id ]) === true) {
					continue;
				}

				$export[ $id ] = ['texte' => $texte, 'fichier' => $fichier];

			}

			echo "\r> ".round( ($avancement++ / count($fichiers)) * 100)." %";

		}

		echo "\r> terminé\n";

		self::exportCsv($export);

	}

	protected static function doImport($optimiseur) {

		echo "import en cours...\n";

		$csv = PATH.'/'.PROJECT.'.csv';

		if (is_file($csv) === false) {
			throw new Exception("Impossible de trouver $csv");
		}

		$delimiter = self::findDelimiter($csv);

		$csv = fopen($csv, 'r');

		while (($ligne = fgetcsv($csv, 0, $delimiter)) !== false) {

			if (count($ligne) < 2) {
				throw new Exception("Mauvais format pour la ligne : ".implode('', $ligne));
			}

			$id = (int) $ligne[0];

			if ($id === 0) {
				continue;
			}

			if (isset($optimiseur[ $id ]) === false) {

				echo "#$id n'existe plus\n";
				continue;

			}

			$texte = empty($ligne[2]) === true ? $ligne[1] : [$ligne[1], $ligne[2]];

			$fichier = $optimiseur[ $id ];

			self::ajouteDictionaire($fichier, $id, $texte);

		}

		echo "\r> terminé\n";

	}

	protected static function doClean($optimiseur) {

		$fichiers = [];

		exec('find '.PATH.'server/i18n/en/ | grep -v ".svn" | grep ".php"', $fichiers);

		if (is_file(PATH.'public/js/i18n/en.js') === true) {

			$fichiers[] = PATH.'public/js/i18n/en.js';

		}

		echo "> ".count($fichiers)." fichiers de traduction détectés\n";

		foreach ($fichiers as $fichier) {

			$dictionary = self::getDictionnaire($fichier);

			foreach ($dictionary as $id => $traduction) {

				if (isset($optimiseur[ $id ]) === false) {

					self::retireDictionnaire($fichier, $id);

				}
				else if (self::getFichierTranslation($optimiseur[ $id ]) !== $fichier) {

					self::retireDictionnaire($fichier, $id);

					$dictionaryAutre = self::getDictionnaire($optimiseur[ $id ]);

					if (isset($dictionaryAutre[ $id ]) === false) {
						self::ajouteDictionaire($optimiseur[ $id ], $id, $traduction);
					}

				}

			}

			if (count($dictionary) === 0) {

				echo "useless file: ".substr($fichier, strlen(PATH.'server/'))."\n";

			}

		}

		echo "\r> terminé\n";

	}

	protected static function doFactorize() {

		echo "factorisation en cours...\n";

		$fichiers = self::scanFichier('___(');

		$total = 0;

		foreach ($fichiers as $fichier) {

			$traduction = self::getTranslation($fichier);

			$hashIndex = [];
			$doublons = [];

			foreach ($traduction as $id => $texte) {

				$hash = is_array($texte) === true ? md5($texte[0].$texte[1]) : md5($texte);

				if (isset($hashIndex[ $hash ]) === true) {

					$doublons[ $id ] = $hashIndex[ $hash ];

				}
				else {

					$hashIndex[ $hash ] = $id;

				}

			}

			if (count($doublons) > 0) {

				$content = file_get_contents($fichier);

				foreach ($doublons as $id => $main) {

					$content = str_replace("___($id, ", "___($main, ", $content);

				}

				file_put_contents($fichier, $content);

			}

			$total += count($doublons);

		}

		echo "\r> terminé\n";

	}

	protected static function ajouteDictionaire($fichier, $id, $texte) {

		$isJs = mb_substr($fichier, -3) === '.js';

		$dictionary = self::getDictionnaire($fichier);

		$dictionary[ $id ] = $texte;

		ksort($dictionary);

		$contenu = $isJs === false ? self::genereContenuPhp($dictionary) : self::genereContenuJs($dictionary);

		$traduction = self::getFichierTranslation($fichier);

		exec('mkdir -p '.dirname($traduction));

		file_put_contents($traduction, $contenu);

	}

	protected static function retireDictionnaire($fichier, $id) {

		$isJs = mb_substr($fichier, -3) === '.js';

		$dictionary = self::getDictionnaire($fichier);

		unset($dictionary[ $id ]);

		ksort($dictionary);

		$contenu = $isJs === false ? self::genereContenuPhp($dictionary) : self::genereContenuJs($dictionary);

		$traduction = self::getFichierTranslation($fichier);

		file_put_contents($traduction, $contenu);

	}

	protected static function genereContenuPhp($dictionary) {

		$liste = '';

		foreach ($dictionary as $id => $texte) {

			$id = (int) $id;
			$texte = self::echapePhp($texte);

			$liste .= "\t$id => $texte,\n";

		}

		return "<?php\n\nTranslation::add([\n$liste]);";

	}

	protected static function genereContenuJs($dictionary) {

		$liste = [];

		foreach ($dictionary as $id => $texte) {

			$texte = self::echapeJs($texte);
			$liste[] = "\t$id: $texte";

		}

		$liste = implode(",\n", $liste);

		return "Ecp.dictionary = {\n$liste\n}";

	}

	protected static function getDictionnaire($fichier) {

		$isJs = mb_substr($fichier, -3) === '.js';

		$fichier = self::getFichierTranslation($fichier);

		if (is_file($fichier) === false) {

			return [];

		}

		return $isJs === false ? self::getDictionnairePhp($fichier) : self::getDictionnaireJs($fichier);

	}

	protected static function getDictionnairePhp($fichier) {

		Translation::cleanDictionnaire();

		require($fichier);

		return Translation::getDictionnaire();

	}

	protected static function getDictionnaireJs($fichier) {

		$actuel = file($fichier);
		$dictionary = [];

		foreach ($actuel as $line => $contenu) {

			$position = mb_strpos($contenu, ':');

			if ($position === false) {
				continue;
			}

			$id = (int) trim(mb_substr($contenu, 0, $position));
			$texte = trim(mb_substr($contenu, $position + 1));

			if (mb_substr($texte, -1) === ',') {
				$texte = mb_substr($texte, 0, -1);
			}

			$dictionary[ $id ] = $texte;

		}

		return $dictionary;

	}

	protected static function getFichierTranslation($fichier) {

		if (strpos($fichier, '/traduction/') !== false) {

			return $fichier;

		}

		if (mb_substr($fichier, -3) === '.js') {

			return PATH.'public/js/i18n/en.js';

		}

		$fichier = mb_substr($fichier, mb_strlen(PATH.'server/'));

		return PATH.'server/i18n/en/'.$fichier;

	}

	protected static function cbScanIncrement($arguments) {

		$id = (int) trim($arguments[1]);

		if ($id !== 0) {

			self::$autoIncrement = max(self::$autoIncrement, $id);

		}

	}

	protected static function getTranslation($fichier) {

		$contenu = file_get_contents($fichier);

		$traduction = [];

		while (true) {

			try {
				$extraction = self::assimileTranslation($contenu);
			}
			catch (TranslationEOFException $e) {
				break;
			}

			if ($extraction === null) {
				continue;
			}

			$traduction[ $extraction['id'] ] = $extraction['texte'];

		}

		return $traduction;

	}

	protected static function assimileTranslation(&$contenu) {

		self::chercheCoupe($contenu, '___(');

		$id = self::parseIdExtraction($contenu);

		if ($id === false) {
			return null;
		}

		self::chercheCoupe($contenu, '"');

		$singulier = self::firstString($contenu);

		if (self::hasPluriel($contenu) === false) {

			return ['id' => $id, 'texte' => $singulier];

		}

		self::chercheCoupe($contenu, '"');

		$pluriel = self::firstString($contenu);

		return ['id' => $id, 'texte' => [$singulier, $pluriel]];

	}

	protected static function firstString(&$contenu) {

		$texte = '';

		while (true) {

			$texte .= self::chercheCoupe($contenu, '"');

			if (mb_substr($texte, -1) !== '\\') {
				break;
			}

		}

		return $texte;

	}

	protected static function hasPluriel($contenu) {

		$contenu = trim($contenu);

		if (mb_substr($contenu, 0, 1) !== ',') {
			return false;
		}

		$contenu = trim(mb_substr($contenu, 1));

		return mb_substr($contenu, 0, 1) === '"';

	}

	protected static function cbCompile($arguments) {

		$avant = $arguments[1];

		self::$autoIncrement++;

		return $avant.'___('.self::$autoIncrement.', "';

	}

	protected static function parseIdExtraction($extraction) {

		$virgule = mb_strpos($extraction, ',');

		if ($virgule === false) {
			return false;
		}

		$id = trim(mb_substr($extraction, 0, $virgule));

		if (is_numeric($id) === false) {
			return false;
		}

		return (int) $id;

	}

	protected static function chercheCoupe(&$contenu, $cherche) {

		$position = mb_strpos($contenu, $cherche);

		if ($position === false) {

			throw new TranslationEOFException();

		}

		$trouve = mb_substr($contenu, 0, $position);

		$contenu = mb_substr($contenu, $position + mb_strlen($cherche));

		return $trouve;

	}

	protected static function exportCsv($export) {

		echo "création du fichier csv...\n";

		$ressource = fopen(PATH.'/'.PROJECT.'.csv', 'w');

		fputcsv($ressource, ['#', 'singulier', 'pluriel', 'fichier']);

		$total = 0;

		foreach ($export as $id => $traduction) {

			$singulier = is_array($traduction['texte']) === true ? $traduction['texte'][0] : $traduction['texte'];
			$pluriel = is_array($traduction['texte']) === true ? $traduction['texte'][1] : '';

			$ligne = [$id, $singulier, $pluriel, mb_substr($traduction['fichier'], mb_strlen(PATH))];

			fputcsv($ressource, $ligne);

			$total += str_word_count($singulier) + str_word_count($pluriel);

		}

		fclose($ressource);

		echo "> $total mots à traduire\n";

	}

	protected static function findDelimiter($file) {

		$file = file($file);
		$line = first($file);

		if (count(explode(',', $line)) >= 3) {
			return ',';
		}

		if (count(explode(';', $line)) >= 3) {
			return ';';
		}

		if (count(explode("\t", $line)) >= 3) {
			return "\t";
		}

		throw new Exception("Format du fichier csv incompatible");

	}

	protected static function getOptimiseur($fichiers) {

		echo "création de l'optimiseur...\n";

		$optimiseur = [];

		foreach ($fichiers as $fichier) {

			$contenu = file_get_contents($fichier);

			$ids = [];
			preg_match_all('/___\((.*),/sU', $contenu, $ids);

			foreach ($ids[1] as $id) {

				if (isset($optimiseur[ $id ]) === true and $optimiseur[ $id ] !== $fichier) {
					exit("fatal: id #$id utilisé dans différents fichiers !\n");
				}

				$optimiseur[ $id ] = $fichier;

			}

		}

		echo "> ".count($optimiseur)." ids indéxés\n";

		return $optimiseur;

	}

	protected static function echapePhp($texte) {

		if (is_array($texte) === true) {

			foreach ($texte as &$chaine) {
				$chaine = self::echapePhp($chaine);
			}

			return '['.implode(', ', $texte).']';

		}

		$texte = str_replace('"', '\\"', $texte);
		$texte = str_replace('$', '\\$', $texte);
		$texte = '"'.$texte.'"';

		return $texte;

	}

	protected static function echapeJs($texte) {

		if (is_array($texte) === true) {

			foreach ($texte as &$chaine) {
				$chaine = self::echapeJs($chaine);
			}

			return '['.implode(', ', $texte).']';

		}

		$texte = str_replace("\n", "\\n", $texte);

		if (mb_substr($texte, 0, 1) !== '"' and mb_substr($texte, 0, 1) !== '[') {
			$texte = '"'.$texte.'"';
		}

		return $texte;

	}

}

class TranslationEOFException extends Exception {}
