<?php

class CLI {

	public static function creation() {

		if (autoArg('--creation') === null) {
			return;
		}

		echo "--- mode creation\n";

		if (autoArg('config') === true) {

			if (is_file(PATH.'config.json') === true) {
				throw new Exception('Impossible, vous avez déjà un fichier config.json');
			}

			if (copy(PATH.'ecp/files/config.sample.json', PATH.'config.json')) {
				echo " fichier config.json créé\n";
			}
			else {
				throw new Exception('Impossible de créer le fichier config.json');
			}

		}

		if (autoArg('projet') === true) {

			exec('tar -xzf '.PATH.'ecp/files/project.sample.tar.gz -C '.PATH);
			echo " nouveau projet créé\n";

		}

		if (autoArg('projet') === null and autoArg('config') === null) {
			echo "utilisation : --creation [config] [projet]\n";
		}

		exit;

	}

	public static function gestion() {

		if (autoArg('--base') !== null) {

			define('CLI_ADMIN', true);

			self::gereBase();

		}

		if (autoArg('--i18n') !== null) {

			define('CLI_ADMIN', true);

			self::gereI18n();

		}

		if (autoArg('--tools') !== null) {

			define('CLI_ADMIN', true);

			self::gereTools();

		}

		if (autoArg('--ws') !== null) {

			define('CLI_ADMIN', false);
			define('MONITORING', false);
			define('MONITORING_SQL', false);

			self::gereWebSocket();

		}

		if (defined('CLI_ADMIN') === false) {

			define('CLI_ADMIN', false);

		}

	}

	protected static function gereBase() {

		echo "--- mode administration de la base de donnée\n";

		if (count($_SERVER['argv']) !== 4) {
			echo "utilisation : --base create|check|repair|optimize|capacity table|ALL\n";
			exit;
		}

		$action = $_SERVER['argv'][2];
		$table = $_SERVER['argv'][3];

		if (in_array($action, ['create', 'check', 'repair', 'optimize', 'capacity', 'date']) === false) {
			throw new Exception("$action n'est pas une action valide.");
		}

		if ($table === 'ALL') {
			$action .= 'All';
		}

		BaseAdmin::$action($table);

		exit;

	}

	protected static function gereI18n() {

		echo "--- mode i18n\n";

		if (count($_SERVER['argv']) < 3) {
			echo "usage: --i18n compile|export|import|clean\n";
			exit;
		}

		$action = $_SERVER['argv'][2];

		if (in_array($action, ['compile', 'export', 'import', 'clean']) === false) {
			throw new Exception("$action n'est pas une action valide.");
		}

		require_once(PATH.'ecp/core/TranslationAdmin.php');

		TranslationAdmin::$action();

		exit;

	}

	protected static function gereTools() {

		echo "--- mode tools\n";

		if (count($_SERVER['argv']) < 3) {
			echo "utilisation : --tools parse\n";
			exit;
		}

		$action = $_SERVER['argv'][2];

		if (in_array($action, ['parse']) === false) {
			throw new Exception("$action n'est pas une action valide.");
		}

		require_once(PATH.'ecp/core/Tools.php');

		Tools::$action();

		exit;

	}

	protected static function gereWebSocket() {

		echo "--- starting websocket server\n";

		require_once(PATH.'ecp/core/WebSocket.php');

		if (autoArg('stop') !== null or autoArg('restart') !== null) {
			WebSocket::kill();
		}

		if (autoArg('start') !== null or autoArg('restart') !== null) {

			if (autoArg('background') === null) {
				WebSocket::run();
			}
			else {
				WebSocket::runBackground();
			}

		}
		else if (autoArg('stop') === null) {
			echo "usage: --ws start|restart|stop [background]\n";
		}

		exit;

	}

}

/**
 * Récupération des arguments de type argument=valeur passés en ligne de commande
 *
 */
function autoArg($nom, $default = null) {

	foreach ($_SERVER['argv'] as $arg) {

		list($nomArg) = explode('=', $arg);

		if ($nom === $nomArg) {

			$valeur = substr($arg, mb_strlen($nomArg) + 1);

			if ($valeur === false) {
				return true;
			}

			return $valeur;

		}

	}

	return $default;

}

function iArg($nom, $index = null) {

	$id = (int) autoArg($nom);

	return $id === 0 ? null : ['id' => $id];

}
