<?php

/**
 * [ BaseAdmin ]
 *
 */

class BaseAdmin {

	private static $valid;

	public static function create($table) {

		$definition = ECP::factory('table', $table);

		$sql = "CREATE TABLE `$table` (\n";

		foreach ($definition->structure as $name => $field) {

			$sql .= "\t".self::transmute($table, $name, $field).",\n";

		}

		if ($definition->taille !== null) {

			$sql .= "\tPRIMARY KEY (`id`),\n";

		}

		foreach ($definition->unique as $field) {

			$sql .= "\t".self::transmuteKey('unique', $field).",\n";

		}

		foreach ($definition->index as $field) {

			$sql .= "\t".self::transmuteKey('index', $field).",\n";

		}

		$sql = substr($sql, 0, -2);

		$sql .= "\n) ENGINE=".$definition->engine." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";

		self::query($table, $sql);

	}

	public static function check($table) {

		$definition = ECP::factory('table', $table);
		$fields = $definition->structure;
		$keys = self::getKeys($definition);

		$createTable = self::query($table, "SHOW CREATE TABLE `$table`", false);
		$createTable = explode("\n", $createTable['Create Table']);

		array_shift($createTable);
		self::$valid = true;

		echo "* $table";

		foreach ($createTable as $createLigne) {

			$createLigne = trim($createLigne);
			$createLigne = preg_replace('/int\([0-9]+\)/', 'int', $createLigne);
			$createLigne = preg_replace('/,$/', '', $createLigne);

			if ($createLigne{0} === '`') {

				$name = substr($createLigne, 1, strrpos($createLigne, '`') - 1);

				if (isset($fields[ $name ]) === true) {

					$valid = self::transmute($table, $name, $fields[ $name ]);

					if ($createLigne !== $valid) {

						self::error($createLigne, $valid);

						self::$fixes[] = "ALTER TABLE `$table` CHANGE `$name` $valid;\n";

					}

					unset($fields[ $name ]);

				}
				else {

					self::error(null, "field `$name` does not exists.");

				}

			}
			else if (strpos($createLigne, 'KEY') !== false) {

				$name = explode('`', $createLigne);
				$name = $name[1];

				if (isset($keys[ $name ]) === true) {

					$valid = $keys[ $name ];

					if ($valid !== $createLigne) {

						self::error($createLigne, $valid);

					}

					unset($keys[ $name ]);

				}
				else {

					self::error(null, "key `$name` does not exists.");

				}

			}
			else {

				$createLigne = preg_replace('/AUTO_INCREMENT=[0-9]+ /', '', $createLigne);

				$valid = ") ENGINE=".$definition->engine." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";

				if ($valid !== $createLigne) {

					self::error($createLigne, $valid);

				}

			}

		}

		foreach ($fields as $name => $field) {

			$valid = self::transmute($table, $name, $field);

			self::error("field `$name` does not exists ($valid)", null);

		}

		foreach ($keys as $name => $valid) {

			self::error("key `$name` does not exists ($valid)", null);

		}

		if (self::$valid === true) {

			echo str_repeat(' ', 30 - mb_strlen($table)).self::console('valid', 'green')."\n";

		}

		return self::$valid;

	}

	static $fixes = [];

	public static function checkAll() {

		$tableSql = [];
		$tableDef = [];
		self::$fixes = [];

		$showTables = ECP::getBase()->query("SHOW TABLES");

		foreach ($showTables as $showTable) {

			$tableSql[] = first($showTable);

		}

		$fichiers = scandir(PATH.'server/tables/');

		foreach ($fichiers as $fichier) {

			if (strpos($fichier, '.php') !== false) {

				$tableDef[] = substr($fichier, 0, -4);

			}

		}

		foreach ($tableDef as $table) {

			if (in_array($table, $tableSql) === true) {

				unset($tableSql[ array_search($table, $tableSql) ]);
				unset($tableDef[ array_search($table, $tableDef) ]);

				self::check($table);

			}

		}

		echo "\n";

		if (count($tableDef) > 0) {
			echo self::console("- missing tables in base: \n\t* ".implode("\n\t* ", $tableDef), 'orange')."\n";
		}

		if (count($tableSql) > 0) {
			echo self::console("- missing tables in project: \n\t* ".implode("\n\t* ", $tableSql), 'orange')."\n";
		}

		foreach (self::$fixes as $fix) {
			echo $fix;
		}

	}

	public static function drop($table) {

		self::query($table, "DROP TABLE IF EXISTS `$table`");

	}

	public static function optimize($table) {

		self::query($table, "OPTIMIZE TABLE `$table`");

	}

	public static function repair($table) {

		self::query($table, "REPAIR TABLE `$table`");

	}

	public static function capacityTable($table) {

		$createTable = self::query($table, "SHOW CREATE TABLE `$table`", false);
		$createTable = explode("\n", $createTable['Create Table']);

		$warning = '';

		foreach ($createTable as $fieldLine) {

			$fieldLine = trim($fieldLine);
			$fieldLine = explode(' ', $fieldLine);

			if ($fieldLine[0]{0} === '`') {

				$field = str_replace('`', '', $fieldLine[0]);
				$type = strtolower(preg_replace('/[^A-Za-z\-]/', '', $fieldLine[1]));
				$unsigned = $fieldLine[2] === 'unsigned';

				if ($field !== 'id') {
					continue;
				}

				$ratio = self::capacityField($table, $field, $type, $unsigned);

				if ($ratio !== false) {

					$color = $ratio > 0.80 ? 'red' : 'orange';
					$pourcent = round($ratio * 100);
					$warning .= self::console("[$field $pourcent%] ", $color);

				}

			}

		}

		if ($warning !== '') {

			echo "$table: $warning\n";

		}

	}

	public static function capacityField($table, $field, $type, $unsigned) {

		$overflow = self::getMax($type);

		if ($overflow === false) {
			return false;
		}

		if ($unsigned === false) {
			$overflow = $overflow / 2;
		}

		$ratio = self::query($table, "SELECT MAX(`$field`) as biggest FROM $table", false);
		$ratio = $ratio['biggest'] / $overflow;

		if ($ratio < 0.4) {
			return false;
		}

		return $ratio;

	}

	public static function capacityAll() {

		$fichiers = scandir(PATH.'server/tables/');

		foreach ($fichiers as $fichier) {

			if (strpos($fichier, '.php') !== false) {

				$table = substr($fichier, 0, -4);

				self::capacityTable($table);

			}

		}

	}

	public static function dateTable($tableName) {

		$table = ECP::factory('table', $tableName);

		foreach ($table->champ as $name => $field) {

			if (strpos($field[0], 'date') !== false) {

				BaseAdmin::dateField($tableName, $name, $field[0]);

			}

		}

	}

	protected static function dateField($table, $field, $type) {

		$value = $type === 'datetime' ? '1000-00-00 00:00:00' : '1000-00-00';

		$hasError =  ECP::getBase()->table($table)
			->where($field, '<', $value)
			->where($field, '!=', null)
			->has();

		if ($hasError) {
			echo self::console("$table.$field", 'red')."\n";
		}

	}

	public static function dateAll() {

		$fichiers = scandir(PATH.'server/tables/');

		foreach ($fichiers as $fichier) {

			if (strpos($fichier, '.php') !== false) {

				$table = substr($fichier, 0, -4);

				self::dateTable($table);

			}

		}

	}

	private static function getMax($type) {

		switch ($type) {

			case 'tinyint':
				return pow(2, 8);

			case 'smallint':
				return pow(2, 16);

			case 'mediumint':
				return pow(2, 24);

			case 'int':
				return pow(2, 32);

			case 'bigint':
				return pow(2, 64);

			case 'float':
			case 'double':
			case 'real':
				return pow(2, 16);

			case 'decimal':
				return pow(10, 6);

		}

		return false;

	}

	private static function query($table, $sql, $index = null) {

		return ECP::getBase()->table($table)->query($sql, $index);

	}

	private static function transmute($table, $name, $field) {

		$canDefault = true;

		switch ($field['type']) {

			case 'bool':
				$type = 'tinyint unsigned';
				break;

			case 'tinyint':
			case 'smallint':
			case 'mediumint':
			case 'int':
			case 'bigint':
			case 'float':
			case 'double':
				$type = strtolower($field['type']);
				$type .= $field['option'] === 'unsigned' ? ' unsigned' : '';
				break;

			case 'decimal':
				$type = strtolower($field['type']).'(8,2)';
				$type .= $field['option'] === 'unsigned' ? ' unsigned' : '';
				break;

			case 'char':
			case 'varchar':
				$type = strtolower($field['type']).'('.$field['option'].') COLLATE utf8mb4_unicode_ci';
				break;

			case 'tinytext':
			case 'text':
			case 'mediumtext':
			case 'longtext':
				$type = strtolower($field['type']).' COLLATE utf8mb4_unicode_ci';
				$canDefault = false;
				break;

			case 'tinyblob':
			case 'blob':
			case 'mediumblob':
			case 'longblob':
				$type = strtolower($field['type']);
				$canDefault = false;
				break;

			case 'date':
			case 'datetime':
			case 'time':
				$type = strtolower($field['type']);
				break;

			case 'year':
				$type = strtolower($field['type'])."(4)";
				break;

			case 'enum':
				$type = "enum('".implode("','", $field['option'])."') COLLATE utf8mb4_unicode_ci";
				break;

			case 'binary':
				$type = 'binary('.$field['option'].')';
				break;

			case 'json':
				$type = 'text COLLATE utf8mb4_unicode_ci';
				$canDefault = false;
				break;

			case 'table':
			case 'info':
				$type = self::getSize($field).' unsigned';
				break;

			default:
				throw new Exception("Le type '".$field['type']."' dans la table `$table` n'est pas valid.");

		}

		$null = $field['null'] === true ? '' : ' NOT NULL';

		$default = '';

		if (($field['null'] === false and $field['default'] === null) === false and $canDefault === true) {

			$valeur = ECP::getBase()->table($table)->encode($name, $field['default']);

			if (is_numeric($valeur) === true) {
				$valeur = "'$valeur'";
			}

			$default = ' DEFAULT '.$valeur;

		}

		$increment = $name === 'id' ? ' AUTO_INCREMENT' : '';

		return "`$name` $type".$null.$default.$increment;

	}

	private static function getKeys($definition) {

		$keys = [];

		if ($definition->taille !== null) {

			$keys['id'] = "PRIMARY KEY (`id`)";

		}

		foreach ($definition->unique as $field) {

			$keys[ $field ] = self::transmuteKey('unique', $field);

		}

		foreach ($definition->index as $field) {

			$keys[ $field ] = self::transmuteKey('index', $field);

		}

		return $keys;

	}

	private static function transmuteKey($type, $field) {

		$key = $type === 'unique' ? 'UNIQUE KEY' : 'KEY';

		return "$key `$field` (`".implode('`,`', explode(':', $field))."`)";

	}

	private static function getSize($field) {

		$definition = ECP::factory($field['type'], $field['option']);

		$valid = ['tinyint', 'smallint', 'mediumint', 'int', 'bigint', null];

		if (in_array($definition->taille, $valid) === true) {

		    return $definition->taille;

		}

		throw new Exception("Capacité of ".$field['type']." ".$field['option']." is not valid.");

	}

	private static function error($base, $projet) {

		if (self::$valid === true) {
			echo "\n";
		}

		self::$valid = false;

		if ($base === null) {

			echo "\t".self::console("project: $projet", 'red')."\n";

			return ;

		}

		if ($projet === null) {

			echo "\t".self::console("base   : $base", 'red')."\n";

			return ;

		}

		$textBase = self::console("base   : $base", 'blue');
		$textProj = self::console("project: $projet", 'cyan');

		echo "\t$textBase\n\t$textProj\n";

	}

	private static function console($text, $color) {

		$colors = [
			'red' => '1;31',
			'green' => '1;32',
			'orange' => '1;33',
			'blue' => '1;34',
			'cyan' => '1;36',
			'white' => '1;37',
		];

		return "\033[".$colors[ $color ]."m".$text."\033[0m";

	}

}
