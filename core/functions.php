<?php

/* -----------------------------------------------------------------------------
					Fonctions pour EcranPlat
----------------------------------------------------------------------------- */

/**
 * Fonctions de base
 */
function autoGet($nom, $defaut = null) {

	return autoArray($_GET, $nom, $defaut);

}

function autoPost($nom, $defaut = null) {

	return autoArray($_POST, $nom, $defaut);

}

function autoCookie($nom, $defaut = null) {

	return autoArray($_COOKIE, $nom, $defaut);

}

function autoSession($nom, $defaut = null) {

	return autoArray($_SESSION, $nom, $defaut);

}

function iGet($nom) {

	$id = (int) autoGet($nom);

	return $id === 0 ? null : ['id' => $id];

}

function iPost($nom) {

	$id = (int) autoPost($nom);

	return $id === 0 ? null : ['id' => $id];

}

function bool($mixed) {

	return $mixed === true or $mixed === 1 or $mixed === '1' or $mixed === 'true';

}

function url($url) {

	if (mb_substr($url, 0, 4) === 'http') {
		return str_replace('&', '&amp;', $url);
	}

	return URL.str_replace('&', '&amp;', $url);
}

function href($url) {

	return 'href="'.url($url).'"';
}

function src($url)	{

	return 'src="'.url($url).'"';
}

function action($url) {

	return 'action="'.url($url).'"';
}

function nLog($fichier, $texte) {

	$fichier = PATH."logs/$fichier".(CLI === true ? '-cli' : '').".log";

	$texte = date('Y-m-d H:i:s')." [".getenv('REMOTE_ADDR')."] : $texte\n";

	@file_put_contents($fichier, $texte, FILE_APPEND);

}

function poseCookie($nom, $valeur, $expiration = 0, $sousDomaine = false) {

	if (is_numeric($expiration) === false) {
		$expiration = strtotime($expiration);
	}

	if (DOMAIN === null) {
		setcookie($nom, $valeur, $expiration, '/');
	}
	else {
		setcookie($nom, $valeur, $expiration, '/', $sousDomaine === true ? '.'.DOMAIN : HOST);
	}

	$_COOKIE[$nom] = $valeur;
}

function enleveCookie($nom) {

	if (autoCookie($nom) !== null) {

		poseCookie($nom, '', time() - 3600);
		poseCookie($nom, '', time() - 3600, true);

	}
}

/**
 * Formulaires et HTML
 */
function htiti($chaine)	{

	return htmlentities($chaine, ENT_QUOTES, 'UTF-8');
}

function boolAttribut($bool, $attribut) {

	if ($bool === true) {
	    return "$attribut=\"$attribut\"";
	}

	return '';
}

/**
 * Gestion des dates
 */
function now($decaleur = null) {

	if ($decaleur === null) {
		return date('Y-m-d H:i:s');
	}

	return date('Y-m-d H:i:s', strtotime($decaleur));

}

function today($decaleur = null) {

	if ($decaleur === null) {
		return date('Y-m-d');
	}

	return date('Y-m-d', strtotime($decaleur));

}

function decaleDate($date, $decaleur) {

	$time = getTime($date);
	$time = strtotime($decaleur, $time);

	$format = strlen($date) === 10 ? 'Y-m-d' : 'Y-m-d H:i:s';

	return date($format, $time);

}

function getTime($date) {

	if (substr($date, 0, 4) === '0000') {
		trigger_error("La date '$date' est certainement une erreur");
	}

	if (strlen($date) === 19) {
		return mktime((int) substr($date, 11, 2), (int) substr($date, 14, 2), (int) substr($date, 17, 2), (int) substr($date, 5, 2), (int) substr($date, 8, 2), (int) substr($date, 0, 4));
	}
	if (strlen($date) === 10) {
		return mktime(0, 0, 0, (int) substr($date, 5, 2), (int) substr($date, 8, 2), (int) substr($date, 0, 4));
	}

	trigger_error("La date '$date' n'a pas un format valide.");

	return false;

}

/**
 * Debug
 */
function debug($var, $type = null) {

	if (CLI === false) {
		echo '<pre>';
	}

	if ($type === false or ($type === null and (is_array($var) or is_object($var)) and empty($var) === false)) {
		print_r($var);
		echo "\n";
	}
	else {
		var_dump($var);
	}

	if (CLI === false) {
		echo '</pre>';
	}

}

function dd() {
	foreach (func_get_args() as $arg) {
		debug($arg);
	}
	exit;
}

/**
 * String
 */
function strCut($string, $taille) {

	if (mb_strlen($string) > $taille) {
		return mb_substr($string, 0, $taille).'...';
	}

	return $string;
}

function getToken($taille) {

	$chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
	$nbChars = mb_strlen($chars);

	$token = '';

	for ($i = 0; $i < $taille; $i++) {
		$token .= mb_substr($chars, mt_rand(0, $nbChars - 1), 1);
	}

	return $token;
}

function s($nombre) {

	if ($nombre >= 2) {
		return 's';
	}

	return '';
}

function base64_url_encode($input) {

	return strtr(base64_encode($input), '+/=', '-_,');

}

function base64_url_decode($input) {

	return base64_decode(strtr($input, '-_,', '+/='));

}

function beginWith($string, $searh) {

	return mb_substr($string, 0, mb_strlen($searh)) === $searh;

}

function strAfter($string, $searh) {

	return mb_substr($string, mb_strpos($string, $searh) + mb_strlen($searh));

}

function strBefore($string, $searh) {

	return mb_substr($string, 0, mb_strpos($string, $searh));

}


/**
 * Tableaux
 */
function autoArray($tableau, $index, $defaut = null) {

	if (array_key_exists($index, $tableau) === false) {
		return $defaut;
	}

	return $tableau[$index];

}

function randArray($tableau) {

	return autoArray($tableau, array_rand($tableau));

}

function first($tableau) {

	$cles = array_keys($tableau);

	if (count($cles) === 0) {
		return null;
	}

	return autoArray($tableau, $cles[0], null);

}

function reindex($tableau, $index) {

	$tableauIndex = [];

	foreach ($tableau as $ligne) {
		$tableauIndex[ $ligne[ $index ] ] = $ligne;
	}

	return $tableauIndex;

}

function getId($variable) {

	return is_array($variable) === true ? $variable['id'] : $variable;

}

function getIds($igLigne, $index) {

	$ids = [];

	foreach ($igLigne as $iLigne) {

		if ($iLigne === null or isset($iLigne[ $index ]) === false) {
			continue;
		}

		$ids[] = (int) getId($iLigne[ $index ]);

	}

	return array_unique($ids);

}

/**
 * Math
 */
function borne($valeur, $min, $max) {

	return min(max($valeur, $min), $max);
}

function truncate($float, $arrondi = 0) {

	if (is_float($float) === false) {
		return $float;
	}

	$explode = explode('.', (string) $float);

	if (count($explode) === 1) {
		return (float) $explode[0];
	}

	if (strpos($float, 'E-') !== false) {
		return (float) 0.0;
	}

	return (float) ($explode[0].'.'.substr($explode[1], 0, $arrondi));
}

function chance($chance, $sur) {

	return mt_rand(1, $sur) <= $chance;
}

/**
 * Javascript
 */
function confirm($texte) {

	$texte = str_replace("'", "\\'", $texte);

	return "onclick=\"return(confirm('$texte'));\" ";
}

/**
 * I18N
 */
function __($texte, $argument = [], $pluriel = null) {

	if ($pluriel !== null) {

		$texte = $pluriel['number'] <= 1 ? $texte : $argument;

		$argument = $pluriel;

	}

	return _traduction($texte, $argument);

}

function ___($id, $texte, $argument = [], $pluriel = null) {

	if ($pluriel !== null) {

		if (Translation::isActive() === true) {

			$texte = Translation::get($id);
			$texte = $pluriel['number'] <= 1 ? $texte[0] : $texte[1];

		}
		else {

			$texte = $pluriel['number'] <= 1 ? $texte : $argument;

		}

		$argument = $pluriel;

	}
	else if (Translation::isActive() === true) {

		$texte = Translation::get($id);

	}

	return _traduction($texte, $argument, $id);

}

function _traduction($texte, $argument, $reference = null) {

	foreach ($argument as $slug => $valeur) {

		$affected = 0;

		$texte = str_replace(":$slug:", $valeur, $texte, $affected);

		if ($affected === 0 and $slug !== 'number') {
			trigger_error("Variable de traduction manquante : #$reference :$slug:");
		}

	}

	return $texte;

}

/**
 * Divers
 */
function getDomaine($url) {

	$host = getHost($url);

	if ($host === null or strpos($host, '.') === false) {
		return null;
	}

	$explode = explode('.', $host);

	// Vérifie que la dernière partie du domaine est une extension (des lettres) :
	if (is_numeric($explode[ count($explode) - 1]) === true) {
		return null;
	}

	return $explode[ count($explode) - 2].'.'.$explode[ count($explode) - 1];

}

function getHost($url) {

	if (($begin = strpos($url, '//')) === false) {
		return null;
	}

	return substr($url, $begin + 2, strpos($url, '/', $begin + 2) - ($begin + 2));

}
