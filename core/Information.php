<?php

abstract class Information extends ECPCore {

	/**
	 * Durée du cache en seconde, utile uniquement si le fichier est configuré sur Memcached.
	 */
	public $cacheExpire = 3600;

	/**
	 * Permet de changer les keys du cache, pour simuler un vidage de cache.
	 */
	public $cacheVersion = 1;

	/**
	 * Capacité du champ sql pour les clées étrangères.
	 */
	public $taille = 'tinyint';

	/**
	 * [ A REDEFINIR ]
	 * A redéfinir, contient les données. Ou utiliser charge() pour les cas plus complexes.
	 */
	protected $igInfo = null;

	/**
	 * Propriétés internes
	 */
	private $optimisateur = [];

	/**
	 * [ CONSTRUCT ]
	 * Charge l'optimisateur
	 */
	public function __construct() {

		if (CLI === true and CLI_ADMIN === true) {

			return false;

		}

		if ($this->igInfo === null) {

			$this->charge();

		}

		foreach ($this->igInfo as $id => $iInfo) {

			if (isset($iInfo['slug']) === false) {
				$iInfo['slug'] = $id;
			}

			$this->optimisateur[ $iInfo['slug'] ] = $id;

		}

	}

	/**
	 * Retourne un élement à partir d'un id ou slug
	 *
	 * @param int | string $valeur - valeur recherchée
	 * @return array - $iInfo | null
	 */
	public function get($valeur) {

		if (is_numeric($valeur) === true) {
			return $this->getById($valeur);
		}

		if (is_string($valeur) === true and isset($this->optimisateur[ $valeur ]) === true) {
			return $this->getById($this->optimisateur[ $valeur ]);
		}

		if (isset($valeur['id']) === true) {
			return $this->getById($valeur['id']);
		}

		if (isset($valeur['slug']) === true and isset($this->optimisateur[ $valeur['slug'] ]) === true) {
			return $this->getById($this->optimisateur[ $valeur['slug'] ]);
		}

		return null;

	}

	/**
	 * Retourne une liste d'élement à partir d'une liste d'id ou slug
	 *
	 * @param array $igInfo - liste d'éléments
	 * @param string | false $index - champ sur lequel indexer (ou pas) les résultats
	 * @return array - liste d'éléments complets
	 */
	public function getMultiple($igInfo, $index = null) {

		$igInfoComplet = [];

		foreach ($igInfo as $iInfo) {

			$iInfo = $this->get($iInfo);

			if ($iInfo === null) {
				continue;
			}

			if ($index === null) {
				$igInfoComplet[ $iInfo['id'] ] = $iInfo;
			}
			else {
				$igInfoComplet[ $iInfo[ $index ] ] = $iInfo;
			}

		}

		return $igInfoComplet;

	}

	/**
	 * Retourne une liste d'élement à partir d'une recherche sur une de ses propriétés
	 *
	 * @param array $champ - champ sur le quel effectuer la recherche
	 * @param string - valeur recherchée
	 * @param string | false $index - champ sur lequel indexer (ou pas) les résultats
	 * @return array - liste d'éléments
	 */
	public function getBy($champ, $valeur, $index = null) {

		$igInfo = [];

		foreach ($this->igInfo as $id => $iInfo) {

			$iInfo['id'] = (int) $id;
			$iInfo = $this->complete($iInfo);

			if (isset($iInfo[ $champ ]) === true and $iInfo[ $champ ] === $valeur) {

				if ($index === false) {
					return $iInfo;
				}

				if ($index === null) {
					$igInfo[] = $iInfo;
				}
				else {
					$igInfo[ $iInfo[$index] ] = $iInfo;
				}

			}

		}

		if ($index === false) {
			return null;
		}

		return $igInfo;

	}

	/**
	 * Retourne tous les éléments
	 *
	 * @param string | false $index - champ sur lequel indexer (ou pas) les résultats
	 * @return array - liste d'éléments
	 */
	public function all($index = null) {

		$igInfo = [];

		foreach ($this->igInfo as $id => $iInfo) {

			$iInfo['id'] = (int) $id;
			$iInfo = $this->complete($iInfo);

			if ($index === null) {
				$igInfo[] = $iInfo;
			}
			else {
				$igInfo[ $iInfo[$index] ] = $iInfo;
			}

		}

		return $igInfo;

	}

	/**
	 * [ PRIVATE ]
	 * Retourne un élément à partir de son id
	 */
	protected function getById($id) {

		if (isset($this->igInfo[ $id ]) === false) {
			return null;
		}

		$iInfo = ['id' => (int) $id] + $this->igInfo[ $id ];
		$iInfo = $this->complete($iInfo);

		return $iInfo;

	}

	/**
	 * [ A REDEFINIR ]
	 * Permet de charger igInfo avec des traitements complexes
	 */
	protected function charge() {

		$this->igInfo = [];

	}

	/**
	 * [ A REDEFINIR ]
	 * Permet de personaliser les sélections dans les classes filles
	 */
	protected function complete($iInfo) {

		return $iInfo;

	}

}

/**
 * Classe qui permet d'accéder aux Information sans charger les fichiers, mais en utilisant Memcached à la place.
 * Pour activer ce système sur vos fichiers, ajoutez les dans la rubrique INFO de votre config.json
 * Cela est utile pour les gros fichiers Information (accélération des requêtes getBy et réduction la consomation de ram).
 */
class ProxyInfo {

	public $information = null;

	public function get($valeur) {

		$key = $this->getKey($valeur);

		return $this->getCache($key, 'get', $valeur);

	}

	public function getMultiple($igInfo, $index = null) {

		$keys = [];

		foreach ($igInfo as $iInfo) {
			$keys[] = $this->getKey($iInfo);
		}

		$igCache = ECP::factory('plugin', 'cache')->getMulti($keys);

		$igInfoComplet = [];

		foreach ($igInfo as $iInfo) {

			$key = $this->getKey($iInfo);

			$iInfo = isset($igCache[ $key ]) === true ? $igCache[ $key ] : $this->get($iInfo);

			if ($index === null) {
				$igInfoComplet[ $iInfo['id'] ] = $iInfo;
			}
			else {
				$igInfoComplet[ $iInfo[ $index ] ] = $iInfo;
			}

		}

		return $igInfoComplet;

	}

	public function getBy($champ, $valeur, $index = null) {

		$key = $this->getKey("$champ-$valeur-$index");

		return $this->getCache($key, 'getBy', $champ, $valeur, $index);

	}

	public function all($index = null) {

		return ECP::factory('info', $this->information)->all($index);

	}

	private function getCache($key, $methode, $arg1 = null, $arg2 = null, $arg3 = null) {

		try {
			return ECP::factory('plugin', 'cache')->get($key);
		}
		catch (CacheException $e) {}

		$Information = ECP::factory('info', $this->information);

		$valeur = $Information->$methode($arg1, $arg2, $arg3);

		ECP::factory('plugin', 'cache')->set($key, $valeur, $Information->cacheExpire);

		return $valeur;

	}

	private function getKey($element) {

		$Information = ECP::factory('info', $this->information);

		return 'info-'.$this->information.'-'.$this->_getKey($element).'-'.$Information->cacheVersion;

	}

	private function _getKey($element) {

		if (is_array($element) === false) {
			return $element;
		}

		if (isset($element['id']) === true) {
			return $element['id'];
		}

		return $element['slug'];

	}

	public function __call($methode, $arguments) {

		return call_user_func_array([ECP::factory('info', $this->information), $methode], $arguments);

	}

}
