<?php

class Translation {

	protected static $nativeLanguage = null;
	protected static $availableLanguage = [];
	protected static $language = null;

	protected static $loadQueue = [];
	protected static $dictionary = [];

	public static function setNativeLanguage($language) {

		self::$nativeLanguage = $language;

	}

	public static function setAvailableLanguage() {

		self::$availableLanguage = func_get_args();

	}

	public static function setLanguage($language) {

		if (self::$nativeLanguage === null or count(self::$availableLanguage) === 0) {
			throw new Exception("Please define nativeLanguage and availableLanguage before setLanguage.");
		}

		if (self::$language !== null) {
			throw new Exception("Can't re-set language.");
		}

		if (in_array($language, self::$availableLanguage) === false) {

			trigger_error("'$language' is not in your available languages. Assuming your first one.");
			$language = first(self::$availableLanguage);

		}

		self::$language = $language;

		require(PATH."ecp/core/i18n/$language.php");

		foreach (self::$loadQueue as $file) {

			self::load($file);

		}

	}

	public static function getBestLanguageFromHeader() {

		if (self::$nativeLanguage === null or count(self::$availableLanguage) === 0) {
			throw new Exception("Please define nativeLanguage and availableLanguage before search best tone.");
		}

		$accept = getEnv('HTTP_ACCEPT_LANGUAGE');
		$accept = explode(',', $accept);

		foreach ($accept as $language) {

			$language = trim($language);
			$language = mb_substr($language, 0, 2);
			$language = strtolower($language);

			if (in_array($language, self::$availableLanguage) === true) {
				return $language;
			}

		}

		return first(self::$availableLanguage);

	}

	public static function getLanguage() {

		if (self::$language === null) {

			trigger_error("Can't getLanguage before setLanguage(). Assuming your first one.");
			return first(self::$availableLanguage);

		}

		return self::$language;

	}

	public static function isActive() {

		return self::getLanguage() !== self::$nativeLanguage;

	}

	public static function get($id) {

		if (isset(self::$dictionary[ $id ]) === false) {

			trigger_error("Translation manquante : #$id");

			return "#$id.";

		}

		return self::$dictionary[ $id ];

	}

	public static function load($file) {

		if (self::$language === null) {
			self::$loadQueue[] = $file;
			return ;
		}

		if (self::isActive() === false) {
			return ;
		}


		$file = mb_substr($file, mb_strlen(PATH.'server/'));
		$file = PATH.'server/i18n/'.self::$language.'/'.$file;

		if (is_file($file) === true) {
			require($file);
		}

	}

	public static function add($dictionary) {

		self::$dictionary += $dictionary;

	}

	public static function getDictionnaire() {

		return self::$dictionary;

	}

	public static function cleanDictionnaire() {

		self::$dictionary = [];

	}

}
