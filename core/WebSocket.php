<?php

/**
 * [ WebSocket ]
 *
 * Serveur de WebSocket non bloquant multi-thread comptabile avec le protocol HyBi 13 - RFC 6455
 * http://tools.ietf.org/html/rfc6455
 *
 * Le serveur gère les app project/socket/*
 *
 * Problèmes connus :
 * ! L'ipc bloque a partir d'environ 8k, il faudra utiliser un autre moyen d'ipc pour les gros paquets.
 *
 * Config :
 * Addresse où les clients vont se connecter. Doit pointer sur l'ip du serveur.
 * On peut préciser le port clients.
 * Doit être le même que le port-server, sauf en cas de routage interne.
 * Grâce au routage interne, vous pouvez router socket.exemple.com:80 sur localhost:8080
 * Et ainsi contourner les firewalls.
 * Port où le serveur va écouter. Les ports < 1024 sont réservés à root.
 * Les autres sont souvent bloqués (défaut 8080).
 */

class WebSocket {

	protected static $clients = [];
	protected static $buffer = [];
	protected static $increment = 1;

	protected static $ipcKey = 121212;
	protected static $longSize = 2048;

	public static function run() {

		self::prepare();
		$pid = self::fork();

		if ($pid === 0) {
			$server = self::start(0, ECP::$config['socket']['server-port']);
			self::serverWorker($server);
		}

		self::console("Server forked on [$pid]\n", 'blue');
		$pid = self::fork();

		if ($pid === 0) {
			self::periodicalWorker();
		}

		self::console("Periodical worker forked on [$pid]\n", 'blue');
		self::mainWorker();

	}

	protected static function mainWorker() {

		while (true) {

			$ipc = self::ipcReceive();

			if ($ipc !== false) {
				self::handleIpc($ipc['client'], $ipc['type'], $ipc['data']);
				self::shareClients();
			}

		}

	}

	protected static function serverWorker($server) {

		while (true) {
			self::accept($server);
		}

	}

	protected static function writingWorker($socket, $id) {

		while (true) {

			$ipc = self::ipcReceive($id);

			if ($ipc !== false) {
				$writed = self::write($socket, $ipc['data']);
			}

		}

	}

	protected static function readingWorker($socket, $id) {

		while (true) {

			$message = self::read($socket);

			if ($message === false) {
				socket_close($socket);
				self::console("Reading worker of client $id died\n", 'red');
				self::ipcToMaster($id, 'die', null);
				exit;
			}

			self::ipcToMaster($id, 'message', $message);

		}

	}

	protected static function periodicalWorker() {

		while (true) {
			self::sleep();
			self::ipcToMaster(null, 'period', null);
		}

	}

	protected static function prepare() {

		if (count(self::getPid()) > 0) {
			self::console("Server is already running.\n", 'red');
			exit;
		}

		pcntl_signal(SIGCHLD, SIG_IGN);
		self::shareClients();

	}

	protected static function start($address, $port) {

		$server = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

		socket_set_option($server, SOL_SOCKET, SO_REUSEADDR, 1);
		socket_bind($server, $address, $port);
		socket_listen($server, 16);

		self::console("Server listening on $address:$port\n", 'green');
		msg_remove_queue(msg_get_queue(self::$ipcKey));

		return $server;

	}

	protected static function accept($server) {

		$socket = socket_accept($server);
		$id = self::$increment;

		self::$increment++;
		self::console("Server has accepted client $id\n", 'green');

		$writer = self::fork();
		$reader = null;

		if ($writer > 0) {

			self::console("Writing worker forked on [$writer]\n", 'blue');
			$reader = self::fork();

			if ($reader > 0) {
				self::console("Reading worker forked on [$reader]\n", 'blue');
				self::ipcToMaster($id, 'accept', ['writer' => $writer, 'reader' => $reader]);
				return true;
			}

		}

		socket_close($server);

		if ($writer === 0) {
			self::writingWorker($socket, $id);
		}

		if ($reader === 0) {
			self::readingWorker($socket, $id);
		}

	}

	protected static function handleIpc($id, $type, $data) {

		if ($type === 'period') {
			self::onPeriod();
			return true;
		}

		if (isset(self::$clients[ $id ]) === false) {

			if ($type !== 'accept') {
				self::console("Ignoring message from unknowed client $id\n", 'orange');
				return false;
			}

			$client = [
				'id' => $id,
				'pid' => $data,
				'ready' => false,
				'ping' => false,
				'create' => now(),
				'update' => time(),
			];

			self::$clients[ $id ] = $client;

			return true;

		}

		if ($type === 'message') {

			if (self::$clients[ $id ]['ready'] === false) {
				self::doAccept($id, $data);
			}
			else {
				self::handleMessage($id, $data);
			}

		}

		if ($type === 'die') {
			self::disconnect($id);
		}

		if ($type === 'ping') {
			self::push(self::$clients[ $id ], 'ping');
			self::$clients[ $id ]['ping'] = true;
		}

		if ($type === 'timeout') {
			self::console("# Timeout ", 'red');
			self::disconnect($id);
		}

	}

	protected static function doAccept($id, $message) {

		try {
			$request = self::parseRequest($message);
		}
		catch (WebSocketException $e) {

			self::console("Requête du client $id incorrect : ".$e->getMessage()."\n", 'red');
			self::ipcToWorker($id, "ECP WebSocket Server refuse connexion : ".$e->getMessage()."\r\n\r\n");
			usleep(1000 * 100);
			self::disconnect($id);

			return false;

		}

		$handcheck = self::buildHandshake($request);
		self::ipcToWorker($id, $handcheck);

		$application = ECP::factory('socket', $request['uri']);

		self::$clients[ $id ]['ready'] = true;
		self::$clients[ $id ]['request'] = $request;
		self::$clients[ $id ]['application'] = $application;

		self::console("Client $id ready on ", 'green');
		self::console('socket/'.$request['uri']."\n", 'white');

		self::onAccept(self::$clients[ $id ]);

	}

	protected static function handleMessage($id, $message) {

		if (isset(self::$buffer[ $id ]) === true) {
			$message = self::$buffer[ $id ].$message;
		}

		$decoded = self::decode($message);

		if ($decoded === false) {
			self::console("Malformated message from client $id\n", 'orange');
			unset(self::$buffer[ $id ]);
			return false;
		}

		if ($decoded['type'] === 'fragment') {
			self::$buffer[ $id ] = $message;
			return false;
		}

		unset(self::$buffer[ $id ]);

		if ($decoded['type'] === 'close') {
			self::console("Client $id send the close frame\n", 'green');
			self::disconnect($id);
			return true;
		}

		$content = $decoded['content'];
		$preview = strlen($content) < self::$longSize ? $content : "long string [".round(strlen($content)/1024)." kb]";

		self::console("> Client $id: ", 'cyan');
		self::console("$preview\n", 'white');

		$json = json_decode($content, true);

		if ($json === 'pong') {
			self::$clients[ $id ]['ping'] = false;
		}
		else {
			self::onMessage(self::$clients[ $id ], $json);
		}

		self::updateTime($id);

	}

	protected static function parseRequest($data) {

		$request = [
			'uri' => null,
			'key' => null,
			'session' => null,
		];

		$parse = [
			'uri' => "/GET \/(.*) HTTP\/1.1\r\n/",
			'session' => "/".SESSION_NAME."=([a-z0-9]+)/",
			'key' => "/Sec-WebSocket-Key: (.*)\r\n/",
		];

		foreach ($parse as $index => $pattern) {

			preg_match($pattern, $data, $match);

			if (count($match) > 0) {
				$request[ $index ] = $match[1];
			}
			else if ($index !== 'session') {
				throw new WebSocketException("can't find '$index' in request.");
			}
			else  {
				self::console("This request does not have headers.\n", 'orange');
			}

		}

		if (file_exists(PATH.'server/sockets/'.$request['uri'].'.php') === false) {
			throw new WebSocketException("Bad request uri, application '".$request['uri']."' does not exist.");
		}

		$request['session'] = ECP::factory('plugin', 'cache')->get('session-'.$request['session'], null);
		return $request;

	}

	protected static function buildHandshake($request) {

		$key = $request['key'].'258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
		$key = sha1($key, true);
		$key = base64_encode($key);

		$response = '';

		$response .= "HTTP/1.1 101 Switching Protocols\r\n";
		$response .= "Upgrade: websocket\r\n";
		$response .= "Connection: Upgrade\r\n";
		$response .= "Sec-WebSocket-Accept: $key\r\n";
		$response .= "\r\n";

		return $response;

	}

	protected static function onPeriod() {

		$applications = [];

		foreach (self::$clients as $client) {

			if ($client['ready'] === true) {

				if (isset($applications[ $client['request']['uri'] ]) === false) {
					ECP::factory('socket', $client['request']['uri'])->onPeriod();
					$applications[ $client['request']['uri'] ] = true;
				}

			}

			if ($client['ping'] === false and $client['update'] < time() - 50) {
				self::ipcToMaster($client['id'], 'ping', null);
			}

			if ($client['ping'] === true and $client['update'] < time() - 60) {
				self::ipcToMaster($client['id'], 'timeout', null);
			}

		}

	}

	protected static function minimizeClients() {

		$clients = [];

		foreach (self::$clients as $client) {

			$minimize = [
				'id' => $client['id'],
				'ready' => $client['ready'],
				'ping' => $client['ping'],
				'update' => $client['update'],
			];

			if ($client['ready'] === true) {
				$minimize['uri'] = $client['request']['uri'];
			}

			$clients[] = $minimize;

		}

		return $clients;

	}

	protected static function disconnect($id) {

		if (isset(self::$clients[ $id ]) === false) {
			self::console("Can not disconnect client ".$id."\n", 'red');
			return false;
		}

		self::console("Client $id disconnected\n", 'red');

		$client = self::$clients[ $id ];
		unset(self::$clients[ $client['id'] ]);

		if ($client['ready'] === true) {
			self::onClose($client);
		}

		posix_kill($client['pid']['writer'], SIGTERM);
		posix_kill($client['pid']['reader'], SIGTERM);

		self::console("Workers [".$client['pid']['writer']."] and [".$client['pid']['reader']."] killed\n", 'blue');

	}

	protected static function read($socket) {

		$buffer = null;
		$data = '';

		while (true) {

			$received = socket_recv($socket, $buffer, 8192, 0);

			if ($received === false or $received === 0) {

				if ($data === '') {
					return false;
				}

				return $data;

			}

			if ($received < 8192) {
				return $data.$buffer;
			}

			$data .= $buffer;

		}

	}

	protected static function decode($data) {

		$decoded = [
			'type' => null,
			'content' => null,
		];

		$firstByte = sprintf('%08b', ord($data{0}));
		$secondByte = sprintf('%08b', ord($data{1}));

		if (substr($firstByte, 0, 4) !== '1000') {
			return false;
		}

		if ($secondByte{0} !== '1') {
			return false;
		}

		$opCode = bindec(substr($firstByte, 4, 4));
		$contentLength = ord($data{1}) & 127;

		switch($opCode) {

			case 1:
				$decoded['type'] = 'text';
				break;

			case 2:
				$decoded['type'] = 'binary';
				return false;

			case 8:
				$decoded['type'] = 'close';
				break;

			case 9:
				$decoded['type'] = 'ping';
				return false;

			case 10:
				$decoded['type'] = 'pong';
				return false;

			default:
				return false;

		}

		if ($contentLength < 126) {

			$mask = substr($data, 2, 4);
			$contentBegin = 6;
			$dataLength = $contentLength + $contentBegin;

		}
		else if ($contentLength === 126) {

			$mask = substr($data, 4, 4);
			$contentBegin = 8;
			$dataLength = bindec(sprintf('%08b', ord($data{2})).sprintf('%08b', ord($data{3}))) + $contentBegin;

		}
		else if ($contentLength === 127) {

			$mask = substr($data, 10, 4);
			$contentBegin = 14;

			$binary = '';
			for($bit = 0; $bit < 8; $bit++) {
				$binary .= sprintf('%08b', ord($data[ $bit + 2 ]));
			}

			$dataLength = bindec($binary) + $contentBegin;

		}
		else {

			return false;

		}

		if (strlen($data) < $dataLength) {
			$decoded['type'] = 'fragment';
			return $decoded;
		}

		$content = '';
		$count = 0;

		for($bit = $contentBegin; $bit < $dataLength; $bit++) {

			$content .= $data{ $bit } ^ $mask{ $count++ % 4 };

		}

		$decoded['content'] = $content;

		return $decoded;

	}

	protected static function encode($data) {

		$length = strlen($data);

		if ($length <= 125) {
			$header = pack('CC', 129, $length);
		}
		else if ($length > 125 && $length < 65536) {
			$header = pack('CCn', 129, 126, $length);
		}
		else {
			$header = pack('CC', 129, 127).'........';

			$header{2} = chr(($length >> 56) & 255);
			$header{3} = chr(($length >> 48) & 255);
			$header{4} = chr(($length >> 40) & 255);
			$header{5} = chr(($length >> 32) & 255);
			$header{6} = chr(($length >> 24) & 255);
			$header{7} = chr(($length >> 16) & 255);
			$header{8} = chr(($length >> 8) & 255);
			$header{9} = chr($length & 255);
		}

		return $header.$data;

	}

	protected static function console($text, $color) {

		$palette = [
			'black' => '0;30',
			'red' => '0;31',
			'green' => '0;32',
			'orange' => '0;33',
			'blue' => '0;34',
			'purple' => '0;35',
			'cyan' => '0;36',
			'white' => '0;37',
		];

		echo "\033[".$palette[ $color ]."m".$text."\033[".$palette['white']."m";

	}

	protected static function fork() {

		return pcntl_fork();

	}

	protected static function ipcToMaster($id, $type, $data) {

		self::doIpcSend(1, ['client' => $id, 'type' => $type, 'data' => $data]);

	}

	protected static function ipcToWorker($id, $data) {

		self::doIpcSend($id + 1, ['type' => 'message', 'data' => $data]);

	}

	protected static function doIpcSend($id, $ipc) {

		$queue = msg_get_queue(self::$ipcKey);

		if (self::isLong($ipc) === true) {

			$long = $ipc;
			$key = 'ipc-'.mt_rand(1000000, 9999999);

			$ipc['type'] = 'long';
			$ipc['data'] = $key;

			ECP::factory('plugin', 'cache')->set($key, $long, 10);

		}

		$result = msg_send($queue, $id, $ipc, true, false);

		if ($result === false) {
			throw new Exception("Oh no ! msg_send failed...");
		}

	}

	protected static function ipcReceive($id = null) {

		$id = $id === null ? 1 : $id + 1;

		$queue = msg_get_queue(self::$ipcKey);
		$ipc = $real = null;

		msg_receive($queue, $id, $real, 10000, $ipc, true, 0);

		if ($ipc === false) {
			return false;
		}

		if ($ipc['type'] === 'long') {

			$ipc = ECP::factory('plugin', 'cache')->get($ipc['data'], null);

			if ($ipc === null) {
				throw new Exception("An ipc long has been lost...\n");
				return false;
			}

		}

		if ($id === 1) {

			if (
				is_array($ipc) === false
				or array_key_exists('client', $ipc) === false
				or array_key_exists('type', $ipc) === false
				or array_key_exists('data', $ipc) === false
			) {
				throw new Exception("Master received invalid ipc : $ipc\n");
			}

			if ($ipc['type'] !== 'period') {

				if (is_int($ipc['client']) === false or $ipc['client'] < 1) {
					throw new Exception("Master received ipc from invalid client : ".$ipc['client']."\n");
				}

			}

		}

		return $ipc;

	}

	protected static function shareClients() {

		file_put_contents('/tmp/websocket-clients', json_encode(self::minimizeClients()));

	}

	protected static function getSharedClients() {

		return json_decode(file_get_contents('/tmp/websocket-clients'), true);

	}

	protected static function isLong($ipc) {

		return is_string($ipc['data']) === true and strlen($ipc['data']) > self::$longSize;

	}

	protected static function sleep() {

		usleep(1000 * 1000);

	}

	public static function push($client, $json) {

		$message = json_encode($json);
		$preview = strlen($message) < self::$longSize ? $message : "long string [".round(strlen($message)/1024)." kb]";

		$client = is_array($client) === true ? $client['id'] : $client;

		if (isset(self::$clients[ $client ]) === false) {
			self::console("Client $client does not exists, can not send message: ", 'orange');
			self::console("$preview\n", 'white');
			return false;
		}

		$client = self::$clients[ $client ];
		self::console('< Client '.$client['id'].': ', 'purple');
		self::console("$preview\n", 'white');
		$message = self::encode($message);

		self::ipcToWorker($client['id'], $message);

	}

	public static function broadcast($json) {

		$message = json_encode($json);
		self::console("< Broadcasting: ", 'orange');
		self::console("$message\n", 'white');
		$message = self::encode($message);

		foreach (self::$clients as $client) {
			self::ipcToWorker($client['id'], $message);
		}

	}

	protected static function write($socket, $data) {

		while (strlen($data) > 0) {

			$writen = socket_write($socket, $data, 8192);

			if ($writen === false) {
				return false;
			}

			$data = substr($data, $writen);

		}

		return true;

	}

	protected static function updateTime($id) {

		self::$clients[ $id ]['update'] = time();

	}

	protected static function onAccept($client) {

		$client['application']->onAccept($client);

	}

	protected static function onMessage($client, $json) {

		$client['application']->onMessage($client, $json);

	}

	protected static function onClose($client) {

		$client['application']->onClose($client);

	}

	public static function kill() {

		$pid = self::getPid();

		if (count($pid) > 0) {
			self::console(count($pid)." process have been detected and killed [".implode(' ', $pid)."]\n", 'orange');
			exec('kill -9 '.implode(' ', $pid));
		}

	}

	public static function runBackground() {

		exec('php -q '.PATH.'ecp/index.php --ws start < /dev/null > '.PATH.'logs/socket.log &');

	}

	protected static function getPid() {

		$process = [];
		$pid = [];

		exec('ps -eo pid,cmd | grep "ecranPlat/core.php --ws" | grep -v grep | grep -v '.getmypid(), $process);

		foreach ($process as $line) {
			$line = explode(' ', trim($line));
			$pid[] = (int) $line[0];
		}

		return $pid;

	}

}

class WebSocketException extends Exception {}
