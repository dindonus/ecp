<?php

class PluginTimer extends Plugin {

	private $result;
	private $actual;
	private $begin;

	public function __construct() {
		$this->result = [];
		$this->actual = null;
		$this->begin = 0;
	}

	public function point($name) {
		$now = microtime(true);
		if ($this->actual !== null) {
			$duration = $now - $this->begin;
			$this->result[$this->actual] += $duration;
		}
		if (isset($this->result[$name]) === false) {
			$this->result[$name] = 0;
		}
		$this->begin = $now;
		$this->actual = $name;
	}

	public function stop() {
		$now = microtime(true);
		if ($this->actual !== null) {
			$duration = $now - $this->begin;
			$this->result[$this->actual] += $duration;
		}
		$this->actual = null;
	}

	public function getResult() {
		$total = 0;
		foreach ($this->result as $name => $time) {
			$total += $time;
		}
		$text = "\n---[ timer ]---\n";
		foreach ($this->result as $name => $time) {
			$pourcent = ( $time / $total ) * 100;
			$pourcent = number_format($pourcent, 2, '.', ' ');
			$time = number_format($time * 1000, 1, '.', ' ');
			$text .= str_pad("$name:", 20);
			$text .= str_pad("$time ms", 15);
			$text .= str_pad("$pourcent%", 15);
			$text .= "\n";
		}
		$total = number_format($total * 1000, 1, '.', ' ');
		$text .= str_pad("total:", 20);
		$text .= str_pad("$total ms", 20);
		$text .= "\n";
		return $text;
	}
}
