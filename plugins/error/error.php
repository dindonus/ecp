<?php

class PluginError extends Plugin {

	protected $nom = 'error';

	public function log($type, $file, $message, $trace) {

		$iError = [
			'type' => $type,
			'message' => mb_substr($message, 0, pow(2, 16)),
			'file' => $file,
			'trace' => $trace,
			'request' => $this->getRequest(),
			'key' => mb_substr(md5($file.$trace), 0, 8),
			'done' => false,
			'ip' => getenv('REMOTE_ADDR'),
			'date' => now(),
		];

		if ($this->table('error')->isAlive() === true) {

			$this->table('error')->insert($iError);

		}
		else {

			nLog($type, $message."\n".$file."\n".$trace);

		}

	}

	public function count($interval = '-1 month') {

		return $this->table('error')
			->where('date', '>', now($interval))
			->where('done', false)
			->count();

	}

	protected function getRequest() {

		return [
			'cli' => CLI,
			'uri' => getenv('REQUEST_URI'),
			'post' => $_POST,
			'session' => isset($_SESSION) ?: null,
		];

	}

}
