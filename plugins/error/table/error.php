<?php

class TableError extends Table {

	public $champ = [
		'type' => ['varchar', 30],
		'message' => ['text'],
		'file' => ['varchar', 250],
		'trace' => ['text'],
		'request' => ['json'],
		'key' => ['char', 8],
		'done' => ['bool'],
		'ip' => ['varchar', 15],
		'date' => ['datetime'],
	];

}
