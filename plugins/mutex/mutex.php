<?php

/**
 * Gestion des mutex grâce à Memcached, via le plugin cache.
 *
 * Les mutex permettent de gérer les accès concurents (système de lock)
 * Ce plugin gère aussi les piles atomiques.
 */
class PluginMutex extends Plugin {

	public function aquiere($mutex, $duree = 10) {

		return $this->plugin('cache')->add('mutex-'.$mutex, 1, $duree);

	}

	public function libere($mutex) {

		return $this->plugin('cache')->supprime('mutex-'.$mutex);

	}

	public function increment($cle, $offset = 1, $security = false) {

		$valeur = $this->plugin('cache')->increment($cle, $offset);

		if ($valeur === false) {

			$succes = $this->plugin('cache')->add($cle, $offset, 3600);

			if ($succes === true) {
				return $offset;
			}

			if ($security === true) {
				throw new Exception("Increment failed !");
			}

			return $this->increment($cle, $offset, true);

		}

		return $valeur;

	}

	public function push($key, $value) {

		$cas = null;

		for ($retry = 0; $retry < 10; $retry++) {

			$list = $this->plugin('cache')->get($key, false, $cas);

			if ($list === false) {

				$list = [$value];

				$succes = $this->plugin('cache')->add($key, $list, 3600);

			}
			else {

				$list[] = $value;

				$succes = $this->plugin('cache')->cas($cas, $key, $list, 3600);

			}

			if ($succes === true) {

				return $list;

			}

			if ($retry === 2) {

				trigger_error("Le push sur la pile '$key' a été difficile.");

			}

		}

		throw new Exception("Le push sur la pile '$key' a échoué.");

	}

	public function clear($key) {

		return $this->plugin('cache')->supprime($key);

	}

}

