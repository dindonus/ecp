<?php

class TableMonitoringPage extends Table {

	public $champ = [
		'page' => ['varchar', '50'],
		'status' => ['enum', ['running', 'done']],
		'time' => ['smallint', 'unsigned', null, true],
		'memory' => ['smallint', 'unsigned', null, true],
		'core' => ['smallint', 'unsigned', null, true],
		'prepare' => ['smallint', 'unsigned', null, true],
		'display' => ['smallint', 'unsigned', null, true],
		'sql' => ['smallint', 'unsigned', null, true],
		'queries' => ['smallint', 'unsigned', null, true],
		'includes' => ['smallint', 'unsigned', null, true],
		'date' => ['datetime'],
	];

}
