<?php

class TableMonitoringQuery extends Table {

	public $champ = [
		'page' => ['table', 'monitoringPage'],
		'query' => ['mediumtext'],
		'time' => ['float'],
		'lock' => ['float', null, null, true],
		'type' => ['varchar', '30'],
		'table' => ['varchar', '30'],
		'write' => ['bool'],
		'index' => ['varchar', 30, null, true],
		'profile' => ['json', null, null, true],
	];

}
