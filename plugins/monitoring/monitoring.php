<?php

class PluginMonitoring extends Plugin {

	protected $nom = 'monitoring';

	protected $iMonitoring = null;

	protected $timePrepare = null;
	protected $timeDisplay = null;

	public function handleStart() {

		$this->iMonitoring = [
			'page' => PAGE,
			'status' => 'running',
			'date' => now(),
		];

		$this->table('monitoringPage')->insert($this->iMonitoring);
		$this->table('monitoringPage')->query('SET PROFILING = 1');

		$this->timePrepare = microtime(true);

	}

	public function handlePrepare() {

		$this->timeDisplay = microtime(true);

	}

	public function handleEnd() {

		$time = microtime(true) - MICRO_TIME;
		$time = $time * 1000;

		$prepare = $this->timeDisplay - $this->timePrepare;
		$prepare = $prepare * 1000;

		$display = microtime(true) - $this->timeDisplay;
		$display = $display * 1000;

		$this->table('monitoringPage')
			->set('status', 'done')
			->set('time', $time)
			->set('memory', memory_get_peak_usage() / 1024)
			->set('core', max(0, $time - ($prepare + $display)))
			->set('prepare', $prepare)
			->set('display', $display)
			->set('sql', floor(ECP::getBase()->time * 1000))
			->set('queries', ECP::getBase()->count - 2)
			->set('includes', count(get_included_files()))
			->update($this->iMonitoring);

		if (MONITORING_SQL === true) {

			$this->monitoringSql($queries);

		}

		ECP::getBase()->stop();

	}

	private function monitoringSql($queries) {

		$profiles = ECP::getBase()->query('SHOW PROFILES');

		foreach ($profiles as &$profile) {
			$profile['Query'] = $this->makeQueryComparaible($profile['Query']);
		}

		foreach ($queries as &$query) {
			$query['profile'] = $this->getProfile($profiles, $query['query']);
		}

		foreach ($queries as $query) {

			$matches = [];
			preg_match('/(SELECT|UPDATE|INSERT|REPLACE|DELETE)\s/', $query['query'], $matches);
			$type = isset($matches[1]) === true ? trim($matches[1]) : '';

			$explain = ECP::getBase()->query('EXPLAIN '.$query['query'], false);

			$iQuery = [
				'page' => $this->iMonitoring,
				'query' => $query['query'],
				'time' => $query['time'] * 1000,
				'lock' => $query['profile']['system-lock'] ?? null,
				'type' => $type,
				'table' => $query['table'],
				'write' => $type !== 'SELECT',
				'index' => $explain['key'],
				'profile' => $query['profile'],
			];

			$this->table('monitoringQuery')->insert($iQuery);

		}

	}

	protected function getProfile($profiles, $query) {

		$id = $this->getQueryId($profiles, $query);

		if ($id === null) {
			return [];
		}

		$status = [];
		$profile = ECP::getBase()->query('SHOW PROFILE FOR QUERY '.$id);

		foreach ($profile as $line) {
			$name = str_replace(' ', '-', $line['Status']);
			$name = strtolower($name);
			$status[ $name ] = $line['Duration'] * 1000;
		}

		return $status;

	}

	protected function getQueryId($profiles, $query) {

		$query = $this->makeQueryComparaible($query);

		foreach ($profiles as $profile) {
			if ($query === $profile['Query']) {
				return (int) $profile['Query_ID'];
			}
		}

		return null;

	}

	protected function makeQueryComparaible($query) {

		$position = mb_strpos($query, 'LIMIT ');
		$query = $position !== false ? mb_substr($query, 0, $position) : $query;
		return trim($query);

	}

}
