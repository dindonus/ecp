<?php

/**
 * Gestion du cache avec Memcached.
 *
 * pour installer memcached :
 * sudo apt-get install memcached php5-memcached
 * puis redemarer apache (ou autre).
 */
class PluginCache extends Plugin {

	private $memcached = null;

	public function __construct() {

		if (class_exists('Memcached') === false) {
			throw new Exception("Veuillez installer Memcached (sudo apt-get install memcached php5-memcached)");
		}

		$params = ECP::$config['memcached'];

		$this->memcached = new Memcached();
		$this->memcached->addServer($params['host'], $params['port']);

		$this->domaine = PROJECT;

	}

	public function set($cle, $valeur, $duree) {

		$result = $this->memcached->set($this->hash($cle), $valeur, $duree);

		return $this->check($result);

	}

	public function get($cle, $defaut = TOKEN, &$cas = null) {

		$valeur = $this->memcached->get($this->hash($cle), null, $cas);

		if ($valeur === false and $this->memcached->getResultCode() === Memcached::RES_NOTFOUND) {

			if ($defaut !== TOKEN) {
				return $defaut;
			}

			throw new CacheException("CacheException ! Utilisez try catch autour de \$this->plugin('cache')->get().");

		}

		return $this->check($valeur);

	}

	public function getMulti($cles) {

		if (count($cles) === 0) {
			return [];
		}

		$hashes = [];
		$correspondance = [];

		foreach ($cles as $cle) {

			$hash = $this->hash($cle);

			$hashes[] = $hash;
			$correspondance[ $hash ] = $cle;

		}

		$founds = $this->memcached->getMulti($hashes);
		$valeurs = [];

		foreach ($founds as $hash => $valeur) {

			$valeurs[ $correspondance[ $hash ] ] = $valeur;

		}

		return $this->check($valeurs);

	}

	public function add($cle, $valeur, $duree) {

		$result = $this->memcached->add($this->hash($cle), $valeur, $duree);

		return $this->check($result);

	}

	public function supprime($cle) {

		$result = $this->memcached->delete($this->hash($cle));

		return $this->check($result);

	}

	public function increment($cle, $offset = 1) {

		$valeur = $this->memcached->increment($this->hash($cle), $offset);

		return $this->check($valeur);

	}

	public function cas($cas, $cle, $valeur, $duree) {

		$result = $this->memcached->cas($cas, $this->hash($cle), $valeur, $duree);

		return $this->check($result);

	}

	public function vide() {

		$result = $this->memcached->flush();

		return $this->check($result);

	}

	protected function hash($cle) {

		return $this->domaine.'-'.md5($cle);

	}

	protected function check($result) {

		if ($result === false) {

			$valide = [Memcached::RES_SUCCESS, Memcached::RES_NOTFOUND, Memcached::RES_NOTSTORED];

			if (in_array($this->memcached->getResultCode(), $valide) === false) {

				trigger_error("Memcached : ".$this->memcached->getResultMessage());

			}

		}

		return $result;

	}

}

class CacheException extends Exception {}
