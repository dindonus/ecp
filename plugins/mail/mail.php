<?php

/**
 * Permet d'envoyer des mails au travers d'un serveur SMTP (uniquement)
 */
class PluginMail extends Plugin {

	protected $server;
	protected $port;
	protected $username;
	protected $password;

	protected $from;
	protected $to = [];
	protected $cc = [];

	protected $header = [];

	protected $stream;
	protected $connectTimeout = 10;
	protected $responseTimeout = 5;

	protected $log = '';
	protected $lastCode;

	public function init($server, $username, $password, $port = 25) {

		$this->server = $server;
		$this->port = $port;
		$this->username = $username;
		$this->password = $password;

		$this->from = null;
		$this->to = [];
		$this->cc = [];

		$this->header = [];

		$this->log = '';

		$this->header('MIME-Version', '1.0');
		$this->header('Content-type', 'text/plain; charset=UTF-8');
		$this->header('Content-Transfer-Encoding', '8bit');

	}

	public function setHtml() {

		$this->header('Content-type', 'text/html; charset=UTF-8');

	}

	public function addTo($address, $name = null) {

		$this->to[] = [$address, $name];

	}

	public function addCc($address, $name = null) {

		$this->cc[] = [$address, $name];

	}

	public function setFrom($address, $name = null) {

		$this->from = [$address, $name];

	}

	public function header($name, $value) {

		$this->header[ $name ] = $value;

	}

	protected function address($address, $short = false) {

		$encode = "<{$address[0]}>";

		if ($short === true or $address[1] === null) {
			return $encode;
		}

		return "\"{$address[1]}\" $encode";

	}

	protected function addressList($list) {

		$encode = [];

		foreach ($list as $address) {

			$encode[] = $this->address($address);

		}

		return implode(",\r\n\t", $encode);

	}

	protected function getHeader($subject) {

		$this->header['From'] = $this->address($this->from);
		$this->header['To'] = $this->addressList($this->to);

		if (count($this->cc) > 0) {
			$this->header['Cc'] = $this->addressList($this->cc);
		}

		$this->header['Subject'] = $subject;
		$this->header['Date'] = date('r');

		$header = '';

		foreach ($this->header as $name => $value) {

			$header .= "$name: $value\r\n";

		}

		return $header;

	}

	protected function push($command) {

		$command = "$command\r\n";

		fputs($this->stream, $command);

		$this->log .= "> $command";

		return $this->read();

	}

	protected function read() {

		$response = '';
		$code = null;

		while (($line = fgets($this->stream, 515)) !== false) {

			$code = (int) mb_substr($line, 0, 3);
			$response .= trim($line)."\r\n";

			if (substr($line, 3, 1) === ' ' and $code !== 334) {
				break;
			}

		}

		$this->lastCode = $code;

		$this->log .= "< $response";

		return $response;

	}

	public function send($subject, $message) {

		$header = $this->getHeader($subject);

		try {

			$this->connect();

			$this->authenticate();

			$this->doSend($header, $message);

		}
		catch (MailException $e) {

			$this->disconnect();

			nLog('mail', $e->getMessage()."\nComplete log:\n".$this->log."\n");

			return false;

		}

		$this->disconnect();

		return true;

	}

	protected function connect() {

		$errno = $errstr = null;

		$this->stream = fsockopen($this->server, $this->port, $errno, $errstr, $this->connectTimeout);

		if ($this->stream === false) {
			throw new MailException("Connect to SMTP server failed");
		}

		stream_set_timeout($this->stream, $this->responseTimeout);

		$this->read();

	}

	protected function disconnect() {

		if ($this->stream === false) {
			return false;
		}

		$this->push('QUIT');

		fclose($this->stream);

	}

	protected function authenticate() {

		$this->push('EHLO localhost');

		$this->push('AUTH LOGIN');
		$this->push(base64_encode($this->username));
		$this->push(base64_encode($this->password));

		if ($this->lastCode !== 235) {
			throw new MailException("Authentication failed");
		}

	}

	protected function doSend($header, $message) {

		$this->push('MAIL FROM: '.$this->address($this->from, true));

		foreach (array_merge($this->to, $this->cc) as $address) {

			$this->push('RCPT TO: '.$this->address($address, true));

		}

		$this->push('DATA');

		$message = $this->encodeMessage($message);

		$this->push("$header\r\n$message\r\n.");

		if ($this->lastCode !== 250) {
			throw new MailException("Send message failed.");
		}

	}

	protected function encodeMessage($message) {

		return str_replace("\n", "\r\n", $message);

	}

}

class MailException extends Exception {}

