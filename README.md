# README #

* EcranPlat provide a very light & fast php backend solution.
* It also provide low level native js libs to help make fluid mobile app.

### Installation ###

`git clone git@bitbucket.org:dindonus/ecp.git`
* php ecp/index --create project
* Copy config.sample.json to config.json

### TODO ###

* Faire le truc iterable a la place de getCurseur
* L'auto create de project
* Chopper plein de functions cool dans Laravel, passer les moches en deprecated
* Gerer Store.js et la config JS
* Essayer de supprimer la partie relations de Base ?