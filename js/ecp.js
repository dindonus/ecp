let Ecp = {

	/**
	 * does client has a touch screen device ?
	 */
	isTouchDevice: null,

	/**
	 * shortcut to natives touch events
	 */
	touch: {},

	/**
	 * actual pointer position (mouse or first finger)
	 */
	pointerX: 0,
	pointerY: 0,
	fingerList: [],

	/**
	 * custom alert()
	 */
	doAlert: null,

	/**
	 * all active language traductions datas
	 */
	dictionary: {},

	/**
	 * dates for client-server synchronisation
	 */
	clientDate: null,

	fetch(page, params) {

		let request = new Request(Config.ecp.url + page, {
			method: 'POST',
			body: JSON.stringify(params || {}),
			headers: new Headers({
				'Content-Type': 'application/json',
				'X-Requested-With': 'XMLHttpRequest'
			})
		})

		let processJson = (json) => {
			if (json.redirection !== undefined) {
				if (json.redirection.substring(0, 7) !== 'http://' && json.redirection.substring(0, 8) !== 'https://') {
					json.redirection = Config.ecp.url + json.redirection
				}
				window.location.replace(json.redirection)
				return false
			}
			if (json.alert !== undefined) {
				Ecp.alert(json.alert)
			}
			return true
		}

		let handleError = (error, page) => {
			let description = Config.ecp.debug === false ? '' : '<hr />' + page + '<br /><br />' + error
			Ecp.alert("Une erreur est survenue lors de la requête !" + description)
		}

		return new Promise((resolve, reject) => {

			fetch(request, {credentials: 'same-origin'})
				.then((response) => {
					response.json()
						.then((data) => {
							if (processJson(data) === true) {
								resolve(data)
							}
						})
						.catch((error) => {
							handleError(error, response.url)
							reject(error)
						})
				})
				.catch((error) => {
					handleError(error, Config.ecp.url + page)
					reject(error)
				})

		})

	},

	/**
	 * return screen position & size
	 */
	getSurface() {

		var surface = {}

		surface.x = (document.documentElement && document.documentElement.scrollLeft) || window.pageXOffset || self.pageXOffset || document.body.scrollLeft
		surface.y = (document.documentElement && document.documentElement.scrollTop) || window.pageYOffset || self.pageYOffset || document.body.scrollTop
		surface.width = (document.documentElement && document.documentElement.clientWidth) || window.innerWidth || self.innerWidth || document.body.clientWidth
		surface.height = (document.documentElement && document.documentElement.clientHeight) || window.innerHeight || self.innerHeight || document.body.clientHeight

		return surface

	},

	/**
	 * update pointer position
	 */
	_updateMouse(event) {

		Ecp.pointerX = event.pageX
		Ecp.pointerY = event.pageY

	},

	_updateFinger(event) {

		var touch = event.touches[0] || event.changedTouches[0]

		Ecp.pointerX = touch.pageX
		Ecp.pointerY = touch.pageY

		Ecp.fingerList = event.touches

	},

	src(url) {

		return 'src="' + Config.ecp.url + url + '"'

	},

	alert(texte) {

		if (Ecp.doAlert === null) {
			alert(texte)
		}
		else {
			Ecp.doAlert(texte)
		}

	},

	getTranslation(id, pluriel) {

		if (Ecp.dictionary[ id ] === undefined) {
			return "Translation js manquante #" + id
		}

		var texte = Ecp.dictionary[ id ]

		if (pluriel !== undefined) {
			return texte[ pluriel === true ? 1 : 0 ]
		}

		return texte

	},

	ajaxForm(eForm) {

		var eSubmit = Js(eForm).find('input[type="submit"], button').first()

		if (eSubmit !== undefined) {
			eSubmit.disabled = true
		}

		params = new FormData() // todo implement this new api

		action = (json) => {
			if (eSubmit !== undefined) {
				eSubmit.disabled = false
			}
		}

		Ecp.ajax(eForm.getAttribute('action'), params, action)

	},

	socket: {

		url: 'ws://localhost:8080',

		start: (scope) => {

			scope._socket = new WebSocket(Config.ecp.socket.url + '/' + scope.application)
			scope._socket.onmessage = (message) => {

				var json = JSON.parse(message.data)

				if (json === 'ping') {
					scope._socket.send(JSON.stringify('pong'))
				}
				else {
					scope.onMessage(json)
				}

			}

			scope._socket.onopen = scope.onOpen
			scope._socket.onclose = scope.onClose
			scope._socket.onerror = scope.onError

		},

		send: (scope, json) => {
			scope._socket.send(JSON.stringify(json))
		}

	},

	getServerDate() {

		var elapse = new Date() - Ecp.clientDate
		var server = new Date()

		server.setTime(Config.ecp.server.date.getTime() + elapse)
		return server

	},

	getSecondTo(date) {

		return parseInt((Ecp.getJsDate(date) - Ecp.getServerDate()) / 1000)

	},

	getJsDate(date) {

		return new Date(
			date.substring(0, 4),
			date.substring(5, 7) - 1,
			date.substring(8, 10),
			date.substring(11, 13),
			date.substring(14, 16),
			date.substring(17, 19)
		)

	}

}

randArray = (array) => {
	return array[ rand(0, array.length - 1) ]
}

autoArray = (array, key, value) => {
	return array[ key ] === undefined ? value : array[ key ]
}

getById = (array, id) => {
	for (var index = 0; index < array.length; index++) {
		if (array[ index ].id === parseInt(id)) {
			return array[ index ]
		}
	}
	return null
}

rand = (min, max) => {
	return parseInt(Math.floor(Math.random() * (max - min + 1)) + min)
}

borne = (value, min, max) => {
	return Math.min(Math.max(value, min), max)
}

ucfirst = (string) => {
	return string.charAt(0).toUpperCase() + string.substr(1)
}

number = (value) => {
	return parseFloat(value).toFixed(2)
}


/**
 * I18N
 */
__ = (texte, argument, pluriel) => {

	if (pluriel !== undefined) {
		texte = pluriel.number <= 1 ? texte : argument
		argument = pluriel
	}

	return _traduction(texte, argument)

}

___ = (id, texte, argument, pluriel) => {

	if (pluriel !== undefined) {

		if (Config.ecp.translation !== null) {
			texte = Ecp.getTranslation(id, pluriel.number > 1)
		}
		else {
			texte = pluriel.number <= 1 ? texte : argument
		}

		argument = pluriel

	}
	else if (Config.ecp.translation !== null) {
		texte = Ecp.getTranslation(id)
	}

	return _traduction(texte, argument)

}

_traduction = (texte, argument) => {

	for (key in argument) {
		texte = texte.replace(':' + key + ':', argument[ key ])
	}

	return texte

}

Date.prototype.toSql = function(millisecond) {
	const split = this.toString().split(' ')
	let string = split[3] + '-'

	string += ('0' + (this.getMonth() + 1)).slice(-2) + '-'
	string += ('0' + this.getDate()).slice(-2) + ' '
	string += split[4]

	if (millisecond === true) {
		string += '.' + this.getMilliseconds()
	}

	return string
}

window.addEventListener('touchstart', Ecp._updateFinger, false)
window.addEventListener('touchmove', Ecp._updateFinger, false)
window.addEventListener('mousemove', Ecp._updateMouse, false)

// TODO : Paul Irish a dit de pas faire ca !
// Remplacer par une methode qui : si posé dit où, sinon, donne last up.
window.addEventListener('touchmove', Ecp._updateFinger, false)

document.addEventListener('DOMContentLoaded', () => {

	Ecp.isTouchDevice = ('ontouchstart' in window || navigator.msMaxTouchPoints !== undefined)

	if (Ecp.isTouchDevice === true) {

		Ecp.touch.start = 'touchstart'
		Ecp.touch.end = 'touchend'
		Ecp.touch.move = 'touchmove'

	}
	else {

		Ecp.touch.start = 'mousedown'
		Ecp.touch.end = 'mouseup'
		Ecp.touch.move = 'mousemove'

	}

	Config.ecp.server.date = Ecp.getJsDate(Config.ecp.server.date)
	Ecp.clientDate = new Date()

	Js('#ecp-profile').click(() => {
		Js('#ecp-profile-sql').show()
	})

}, false)
