
var Drag = {

	init: function(selector, option) {

		var option = Js.extend({
			revert: true,
			start: null,
			move: null,
			stop: null,
			drop: null,
			over: null,
			leave: null,
			droppable: null,
			tolerance: 10,
			slide: 2,
			gapX: 0,
			gapY: 0,
			stopPropagation: true
		}, option);

		Js(selector).each(function(element) {

			var active = false;
			var originPosition = Animator.position(element);

			var startFinger = null;
			var startTranslation = null;
			var previousFinger = null;

			var previousOffset = [];
			var timeoutReference = null;
			var droppableOver = null;

			var onStart = function(event) {

				if (option.stopPropagation === true) {

					event.preventDefault();
					event.stopPropagation();

				}

				startFinger = Js.getPointerFromEvent(event);
				originPosition = Animator.position(element);
				startTranslation = Animator.translation(element);

				previousOffset = [];
				previousOffset.push({
					time: parseInt(Date.now()),
					x: startFinger.pageX,
					y: startFinger.pageY
				});

				previousFinger = {
					x: Ecp.pointerX,
					y: Ecp.pointerY
				}

				active = false;

				Js(window).bind('touchmove mousemove', onMove);
				Js(window).bind('touchend mouseup', onDrop);

				onMove(event);

			};

			var onMove = function(event) {

				if (option.stopPropagation === true) {
					//event.preventDefault();
					//event.stopPropagation();
				}

				var pointer = Js.getPointerFromEvent(event);
				var distance = Math.abs(pointer.x - startFinger.x) + Math.abs(pointer.y - startFinger.y);

				if (active === false && distance < option.tolerance) {
					return ;
				}

				if (active === false) {

					if (option.start !== null) {
						option.start(event, element);
					}

					active = true;

				}

				var vector = {
					x: pointer.x - startFinger.x + startTranslation.x + option.gapX,
					y: pointer.y - startFinger.y + startTranslation.y + option.gapY
				};

				Animator.translate(element, vector.x, vector.y);

				previousOffset.push({
					time: parseInt(Date.now()),
					x: pointer.x,
					y: pointer.y
				});

				if (previousOffset.length > 100) {
					previousOffset.shift();
				}

				if (option.move !== null) {
					option.move(event);
				}

				if (option.over !== null) {

					var droppable = Drag._getDroppable(pointer);

					if (droppable !== null && droppableOver === null) {

						option.over(event, element, droppable);
						droppableOver = droppable;

					}

					if (droppable === null && droppableOver !== null) {

						option.leave(event, element, droppableOver);
						droppableOver = null;

					}

				}

			};

			var onDrop = function(event) {

				Js(window).unbind('touchmove mousemove', onMove);
				Js(window).unbind('touchend mouseup', onDrop);

				if (active === false) {
					return ;
				}

				active = false;

				var pointer = Js.getPointerFromEvent(event);
				var dropped = Drag._getDroppable(pointer);

				if (dropped === null) {

					if (option.revert === true) {

						Animator.slide(element, originPosition.left, originPosition.top, 0.35);

					}
					else {

						previousFinger = Drag._getPreviousFinger(previousOffset);

						if (previousFinger !== null) {

							var vectorX = (pointer.x - previousFinger.x) * option.slide;
							var vectorY = (pointer.y - previousFinger.y) * option.slide;
							var position = Animator.position(element);

							var offset = {
								x: position.left + vectorX,
								y: position.top + vectorY
							};

							Animator.slide(element, offset.x, offset.y, 0.5, 'cubic-bezier(0.390, 0.575, 0.565, 1.000)');

						}

					}

				}

				if (option.stop !== null) {
					option.stop(event, element);
				}

				if (dropped !== null && option.drop !== null) {
					option.drop(event, element, dropped);
				}

			};

			Js(element).bind(Ecp.touch.start, onStart);

		});

		if (option.droppable !== null) {

			Drag.droppableList = [];
			Drag._setDroppable(option.droppable);

		}

	},

	_getPreviousFinger: function(previousOffset) {

		let found = previousOffset.find(function(offset) {

			if (offset.time > Date.now() - 100) {

				return offset;

			}

		});

		return found === undefined ? null : found;

	},

	droppableList: [],

	_setDroppable: function(selector) {

		Js(selector).each(function(element) {

			var offset = Js(element).offset();
			var style = Js(element).getStyle();

			Drag.droppableList.push({
				element: element,
				left: offset.left,
				top: offset.top,
				right: offset.left + parseInt(style.width),
				bottom: offset.top + parseInt(style.height)
			});

		});

	},

	_getDroppable: function(pointer) {

		const found = Drag.droppableList.find(box =>
			(pointer.x >= box.left && pointer.x <= box.right && pointer.y >= box.top && pointer.y <= box.bottom)
		);

		return found === undefined ? null : found.element;

	}

}
