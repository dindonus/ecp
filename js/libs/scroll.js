/**
 * Scroll.js - Fast and cool scrolling with parallax!
 * Requiere : Native.js & Animator.js
 *
 * TODO :
 * - Smoth overflow reset
 */

var Scroll = {

	element: null,
	container: null,

	sElement: null,
	sContainer: null,

	startFinger: null,
	startTranslation: null,
	previousOffset: [],

	layers: [],

	init: function(selector, handleEvent) {

		Scroll.element = Js(selector).first();
		Scroll.container = Scroll.element.parentNode;

		Js(Scroll.element).css({
			position: 'absolute',
			left: 0,
			top: 0
		});

		Animator.translate(Scroll.element, 0, 0, undefined, undefined, undefined, true);

		Scroll.resize();

		if (handleEvent !== false) {

			Js(selector).bind('touchstart mousedown', Scroll.touchStart);

		}

		if (Ecp.isTouchDevice === false) {

			// Js(selector).bind('mousewheel DOMMouseScroll', Scroll.mouseWheel);

		}

	},

	resize: function() {

		if (Scroll.element !== null) {

			Scroll.sElement = Scroll._getSurface(Scroll.element);
			Scroll.sContainer = Scroll._getSurface(Scroll.container);

		}

	},

	parallax: function(selector, coefficient) {

		var layer = {
			element: Js(selector).first(),
			coefficient: coefficient
		};

		Scroll.layers.push(layer);

	},

	touchStart: function(event) {

		if (event.touches !== undefined && event.touches.length > 1) {
			return ;
		}

		event.preventDefault();
		event.stopPropagation();

		Scroll._stop(Scroll.element);

		Scroll.startFinger = Js.getPointerFromEvent(event);
		Scroll.startTranslation = Animator.translation(Scroll.element);
		Scroll.previousOffset = [];

		Js(window).bind('touchmove mousemove', Scroll.touchMove);
		Js(window).bind('touchend mouseup', Scroll.touchEnd);

	},

	touchMove: function(event) {

		event.preventDefault();
		event.stopPropagation();

		var finger = Js.getPointerFromEvent(event);

		var vector = {
			x: finger.x - Scroll.startFinger.x + Scroll.startTranslation.x,
			y: finger.y - Scroll.startFinger.y + Scroll.startTranslation.y
		};

		vector.x = Math.min(0, vector.x);
		vector.y = Math.min(0, vector.y);

		vector.x = Math.max(Scroll.sContainer.width - (Scroll.sElement.width), vector.x);
		vector.y = Math.max(Scroll.sContainer.height - (Scroll.sElement.height), vector.y);

		Animator.translate(Scroll.element, vector.x, vector.y, undefined, undefined, undefined, true);
		Scroll._translateAllLayers(vector);

		Scroll.previousOffset.push({
			time: parseInt(Date.now()),
			x: finger.x,
			y: finger.y
		});

		if (Scroll.previousOffset.length > 100) {
			Scroll.previousOffset.shift();
		}

	},

	touchEnd: function(event) {

		Js(window).unbind('touchmove mousemove', Scroll.touchMove);
		Js(window).unbind('touchend mouseup', Scroll.touchEnd);

		var finger = Js.getPointerFromEvent(event)
		var previousFinger = Scroll._getPreviousFinger(Scroll.previousOffset);

		if (previousFinger !== null) {

			var vectorX = (finger.x - previousFinger.x) * 3;
			var vectorY = (finger.y - previousFinger.y) * 3;
			var position = Animator.position(Scroll.element);

			var offset = {
				x: position.left + vectorX,
				y: position.top + vectorY
			};

			Scroll.slide(offset.x, offset.y, 1);

		}

	},

	mouseWheel: function(event) {

		var isUp = event.detail < 0 || event.wheelDelta > 0;
		var translation = Animator.translation(Scroll.element);
		var y = isUp === true ? translation.y + 200 : translation.y - 200;

		Scroll.slide(translation.x, y, 0.25);

	},

	_getSurface: function(element, isGlobal) {

		var offset = Js(element).offset(); // Translate must be include.

		if (isGlobal === true) {

			offset.left = offset.left - Scroll.sContainer.x;
			offset.top = offset.top - Scroll.sContainer.y;

		}

		return {x: offset.left, y: offset.top, width: element.offsetWidth, height: element.offsetHeight};

	},

	_getPreviousFinger: function(previousOffset) {

		return previousOffset.get(function(offset) {

			if (offset.time > Date.now() - 100) {

				return offset;

			}

		});

	},

	slide: function(x, y, duration, ease, callback) {

		x = Math.min(0, x);
		y = Math.min(0, y);

		x = Math.max(Scroll.sContainer.width - (Scroll.sElement.width), x);
		y = Math.max(Scroll.sContainer.height - (Scroll.sElement.height), y);

		var ease = ease === undefined ? 'cubic-bezier(0.390, 0.575, 0.565, 1.000)' : ease;
		var position = Animator._position(Scroll.element);

		var vector = {
			x: Math.round(x) - position.left,
			y: Math.round(y) - position.top
		};

		Animator.translate(Scroll.element, vector.x, vector.y, duration, ease, callback, true);

		Scroll._slideAllLayers(vector, duration, ease);

	},

	stop: function() {

		Animator.stop(Scroll.element);
		Scroll._stopAllLayers()

	},

	_stop: function() {

		var vector = Animator.translation(Scroll.element);

		Animator.translate(Scroll.element, vector.x, vector.y, undefined, undefined, undefined, true);

	},

	_translateAllLayers: function(vector) {

		Scroll.layers.forEach(function(layer) {

			Animator.translate(layer.element, vector.x * layer.coefficient, vector.y * layer.coefficient, undefined, undefined, undefined, true);

		});

	},

	_slideAllLayers: function(vector, duration, ease) {

		Scroll.layers.forEach(function(layer) {

			Animator.translate(layer.element, vector.x * layer.coefficient, vector.y * layer.coefficient, duration, ease, undefined, true);

		});

	},

	_stopAllLayers: function() {

		Scroll.layers.forEach(function(layer) {

			Animator.stop(layer.element);

		});

	},

}
