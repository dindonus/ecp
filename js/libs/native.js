
var Js = function(selector) {
	return new JsFactory(selector);
};

function JsFactory(selector) {

	this.collection = Js.getAll(selector);

	return this;

}

JsFactory.prototype = {

	each: function(code) {

		this.collection.forEach(function(element) {

			code(element);

		});

		return this;

	},

	first: function() {

		return this.collection[0];

	},

	get: function() {

		return this.collection;

	},

	count: function() {

		return this.collection.length;

	},

	find: function(selector) {

		this.collection = Js.find(this.first(), selector);

		return this;

	},

	bind: function(eventList, code) {

		var eventList = eventList.split(' ');

		this.collection.forEach(function(element) {

			eventList.forEach(function(event) {

				element.addEventListener(event, code, false);

			});

		});

		return this;

	},

	unbind: function(eventList, code) {

		var eventList = eventList.split(' ');

		this.collection.forEach(function(element) {

			eventList.forEach(function(event) {

				element.removeEventListener(event, code, false);

			});

		});

		return this;

	},

	one: function(eventList, code) {

		var parentCode = function(event) {

			event.target.removeEventListener(event.type, parentCode, false);

			code(event);

		}

		this.bind(eventList, parentCode);

		return this;

	},

	click: function(code) {

		this.bind('click', code);

		return this;

	},

	show: function() {

		this.collection.forEach(function(element) {

			if (['hidden', 'none'].indexOf(Js._getStyle(element).display) !== -1) {
				element.style.display = 'block';
			}

			if (element.style.transition.indexOf('opacity') !== -1) {
				element.style.transition = '';
			}

			element.hidden = false;
			element.style.opacity = 1;

		});

		return this;

	},

	hide: function() {

		this.collection.forEach(function(element) {

			element.hidden = true;
			element.style.display = 'none';

		});

		return this;

	},

	fadeIn: function(duration) {

		this.show();
		this.css({opacity: 0}).forceStyle();

		this.css({
			'transition': 'opacity ' + duration + 's linear',
			'-webkit-transition': 'opacity ' + duration + 's linear',
			'opacity': 1
		});

		return this;

	},

	fadeOut: function(duration) {

		this.css({
			'transition': 'opacity ' + duration + 's linear',
			'-webkit-transition': 'opacity ' + duration + 's linear',
			'opacity': 0
		});

		window.setTimeout(function(collection) {

			collection.forEach(function(element) {

				element.hidden = true;
				element.style.display = 'none';

			});

		}, duration * 1000 + 100, this.collection);

		return this;

	},

	addClass: function(name) {

		this.collection.forEach(function(element) {

			element.classList.add(name);

		});

		return this;

	},

	removeClass: function(name) {

		this.collection.forEach(function(element) {

			element.classList.remove(name);

		});

		return this;

	},

	forceStyle: function() {

		this.collection.forEach(function(element) {

			// This is layout trashing...
			window.getComputedStyle(element).display;

		});

		return this;

	},

	remove: function() {

		this.collection.forEach(function(element) {

			element.parentNode.removeChild(element);

		});

		return this;

	},

	tap: function(code) {

		this.collection.forEach(function(element) {

			var startPosition = {x: 0, y: 0};
			var startDate = Date.now();

			element.addEventListener(Ecp.touch.start, function(event) {

				startPosition = Js.getPointerFromEvent(event);
				startDate = Date.now();

			}, false);

			element.addEventListener(Ecp.touch.end, function(event) {

				var pointer = Js.getPointerFromEvent(event);

				if (Math.abs(pointer.x - startPosition.x) > 10 || Math.abs(pointer.y - startPosition.y) > 10) {
					return false;
				}

				if (Date.now() - startDate < 750) {

					event.target = this;
					code(event, this);

				}

			}, false);

		});

		return this;

	},

	hold: function(code, delay) {

		var holdEnd = function(startPosition, event) {

			if (Math.abs(Ecp.pointerX - startPosition.x) <= 10 && Math.abs(Ecp.pointerY - startPosition.y) <= 10) {
				code(event);
			}

		}

		this.collection.forEach(function(element) {

			var startPosition = {x: 0, y: 0};
			var timeout = null;

			element.addEventListener(Ecp.touch.start, function(event) {

				startPosition = Js.getPointerFromEvent(event);
				timeout = window.setTimeout(holdEnd, delay, startPosition, event);

			}, false);

			element.addEventListener(Ecp.touch.end, function(event) {

				window.clearTimeout(timeout);

			}, false);

		});

		return this;

	},

	html: function(html) {

		if (html === undefined) {

			return this.collection[0].innerHTML;

		}

		this.collection.forEach(function(element) {

			element.innerHTML = html;

		});

		return this;

	},

	append: function(html) {

		this.collection.forEach(function(element) {

			element.innerHTML = element.innerHTML + html;

		});

		return this;

	},

	data: function(name, value) {

		if (value === undefined) {

			return this.collection[0].dataset[ name ];

		}

		this.collection.forEach(function(element) {

			element.dataset[ name ] = value;

		});

		return this;

	},

	css: function(css) {

		this.collection.forEach(function(element) {

			for (property in css) {

				element.style[ property ] = Js._formate(property, css[ property ]);

			}

		});

		return this;

	},

	getStyle: function() {

		return Js._getStyle(this.collection[0]);

	},

	width: function() {

		return parseInt(this.getStyle().width);

	},

	height: function() {

		return parseInt(this.getStyle().height);

	},

	offset: function() {

		var surface = this.collection[0].getBoundingClientRect();

		return {
			top: surface.top + (window.scrollY || window.pageYOffset),
			left: surface.left + (window.scrollX || window.pageXOffset)
		};

	}

};

JsFactory.prototype.constructor = JsFactory;

Js.ready = function(code) {

	document.addEventListener('DOMContentLoaded', code, false);

};

Js.getAll = function(selector) {

	if (typeof selector === 'string') {

		return Js.find(document, selector);

	}

	return selector.collection !== undefined ? selector.collection : [selector];

};

Js.find = function(element, selector) {

	var nodeList = element.querySelectorAll(selector);
	var list = [];

	for (var index = 0; index < nodeList.length; index++) {

		list.push(nodeList[ index ]);

	}

	return list;

};

Js.getPointerFromEvent = function(event) {

	if (event.originalEvent !== undefined) {
		return this.getPointerFromEvent(event.originalEvent);
	}

	var pointer = event;

	if (Ecp.isTouchDevice === true) {
		var pointer = event.changedTouches[0] || event.touches[0];
	}

	return {x: pointer.pageX, y: pointer.pageY};

};

Js.extend = function(parent, child) {

	for (property in parent) {

		if (child[ property ] === undefined) {
			child[ property ] = parent[ property ];
		}

	}

	return child;

};

Js._getStyle = function(element) {

	return window.getComputedStyle(element);

}

Js._formate = function(property, value) {

	if (new Array('top', 'right', 'bottom', 'left', 'width', 'height').indexOf(property) !== -1) {

		if (typeof value === 'number' || parseFloat(value) + '' === value) {

			return Math.round(value) + 'px';

		}

	}

	return value;

};
