
var Animator = {

	slide: function(selector, x, y, duration, ease, callback) {

		var ease = ease === undefined ? 'ease' : ease;

		Js(selector).each(function(element) {

			var position = Animator._position(element);

			var vector = {
				x: Math.round(x) - position.left,
				y: Math.round(y) - position.top
			};

			Animator.translate(element, vector.x, vector.y, duration, ease, callback, false);

		});

	},

	stop: function(selector, x, y) {

		Js(selector).each(function(element) {

			if (x === undefined || y === undefined) {

				var vector = Animator.translation(element);

			}
			else {

				var position = Animator._position(element);

				var vector = {
					x: Math.round(x) - position.left,
					y: Math.round(y) - position.top
				};

			}

			Animator.translate(element, vector.x, vector.y);

		});

	},

	fix: function(element) {

		var position = Animator.position(element);

		Animator.translate(element, 0, 0);

		Js(element).css({
			left: position.left,
			top: position.top
		});

	},

	position: function(element) {

		// Return position including translation
		// getBoundingClientRect can also be a solution?
		var position = Animator._position(element);
		var translation = Animator.translation(element);

		return {
			left: position.left + translation.x,
			top: position.top + translation.y
		}

	},

	_position: function(element) {

		// Return position without translation
		// Warning: work only if position was previously define in px!
		var style = Js(element).getStyle();
		var left = parseFloat(style.left);
		var top = parseFloat(style.top);

		left = isNaN(left) === true ? 0 : left;
		top = isNaN(top) === true ? 0 : top;

		return {
			left: Math.round(left),
			top: Math.round(top)
		};

	},

	translation: function(element) {

		var style = Js(element).getStyle();
		var transform = style.transform || style.webkitTransform;
		var matrix = transform.split(',');

		if (matrix[0].indexOf('matrix3d') > -1) {

			if (matrix[12] !== undefined && matrix[13] !== undefined) {

				return {
					x: Math.round(parseFloat(matrix[12])),
					y: Math.round(parseFloat(matrix[13]))
				}

			}

		}
		else if (matrix[4] !== undefined && matrix[5] !== undefined) {

			return {
				x: Math.round(parseFloat(matrix[4])),
				y: Math.round(parseFloat(matrix[5]))
			}

		}

		return {x: 0, y: 0};

	},

	translate: function(selector, x, y, duration, ease, callback, acceleration) {

		var acceleration = acceleration === true ? ' translateZ(1px)' : ''; // broke z-index, use only for large parts.
		var transition = duration === undefined ? 'none' : 'transform ' + duration + 's ' + ease;

		x = Math.round(x);
		y = Math.round(y);

		Js(selector).each(function(element) {

			var translation = Animator.translation(element);

			if (translation.x === x && translation.y === y && callback !== undefined) {

				callback(element);
				return ;

			}

			Js(element).css({
				transition: transition,
				transform: 'translate(' + x + 'px, ' + y + 'px)' + acceleration
			});

			if (callback !== undefined) {

				Js(element).one('transitionend', function(event) {

					if (event.target === element) {
						callback(element);
					}

				});

			}

		});

	},

	rotate: function(selector, angle, duration, ease, conserve) {

		if (conserve !== true) {

			Js(selector).css({
				transition: 'none',
				transform: 'none'
			});

		}

		Js(selector).forceStyle();

		var transition = duration === undefined ? 'none' : 'transform ' + duration + 's ' + ease;

		Js(selector).css({
			transition: transition,
			transform: 'rotate(' + angle + 'deg)'
		});

	},

	rotlate: function(selector, x, y, angle, duration, ease) {

		Js(selector).forceStyle();

		var transition = duration === undefined ? 'none' : 'transform ' + duration + 's ' + ease;

		Js(selector).css({
			transition: transition,
			transform: 'translate(' + x + 'px, ' + y + 'px) rotate(' + angle + 'deg)'
		});

	}

}
