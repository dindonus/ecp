var Audio = {

	context: null,
	buffer: {},
	isMute: false,

	init: function() {

		window.AudioContext = window.AudioContext || window.webkitAudioContext;
		Audio.context = new AudioContext();

	},

	play: function(file, option) {

		if (Audio.isMute === true) {
			return false;
		}

		if (Audio.context === null) {
			Audio.init();
		}

		option = option || {};

		if (Audio.buffer[ file ] === undefined) {

			Audio.load(file, option);

		}

		if (Audio.isReady(file) === true) {

			Audio.buffer[ file ].source = Audio.playBuffer(Audio.buffer[ file ].buffer, option);

		}

	},

	isReady: function(file) {

		return Audio.buffer[ file ] !== undefined && Audio.buffer[ file ] !== 'loading';

	},

	stop: function(file) {

		if (Audio.isReady(file) === true) {
			Audio.buffer[ file ].source.stop();
		}

	},

	load: function(file, option) {

		Audio.buffer[ file ] = 'loading';

		Audio.fetchData(file, function(data) {

			Audio.context.decodeAudioData(data, function(buffer) {

				var source = Audio.playBuffer(buffer, option);

				Audio.buffer[ file ] = {buffer: buffer, file: file, source: source};

			}, function() {});

		});

	},

	fetchData: function(file, completed) {

		// var data = sessionStorage.getItem(file);
		var data = null;

		if (data !== null) {

			completed(data);

		}
		else {

			var url = Config.ecp.url + 'public/audio/' + file + '.mp3';
			var request = new XMLHttpRequest();

			request.open('GET', url, true);
			request.responseType = 'arraybuffer';

			request.onload = function() {

				data = request.response;

				// sessionStorage.setItem(file, data);
				completed(data);

			}

			request.send();

		}

	},

	playBuffer: function(buffer, option) {

		var source = Audio.context.createBufferSource();
		source.connect(Audio.context.destination);
		source.buffer = buffer;

		if (option.loop === true) {
			source.loop = true;
		}

		source.start(0);

		if (option.volume !== undefined) {

			var gainNode = Audio.context.createGain();
			gainNode.connect(Audio.context.destination);
			source.connect(gainNode);

			gainNode.gain.value = option.volume;

		}

		return source;

	},

	mute: function() {

		Audio.isMute = true;

	},

	unmute: function() {

		Audio.isMute = false;

	}

}
